Auto mount encrypted partitions at boot
check the name of the encrypted partition:
```
lsblk
```

for example, lets assume it's sda1
next find the UUID of the encrypted drive:
```
sudo cryptsetup luksUUID /dev/sda1
```

let's assume it returned:
fd3c01ad-0e59-4bc1-9bda-7c61e00b36cf
Time to edit the /etc/crypttab file:
```
sudo vim /etc/crypttab
```

add the following line and save it:
```
sda1 /dev/disk/by-uuid/<UUID of block device> none nofail luks
```

it should look something like this
sda1 /dev/disk/by-uuid/fd3c01ad-0e59-4bc1-9bda-7c61e00b36cf none nofail luks
format of the entry:

sda1 is the name of the encrypted drive

fd3c01ad-0e59-4bc1-9bda-7c61e00b36cf is the UUID we got from sudo cryptsetup luksUUID /dev/sda1

none - means we do not use a key file but a password or pass phrase

nofail - must have in case the drives can not mounted and the system can continue to boot

luks - is the disk encryption type

source:
```
https://averagelinuxuser.com/auto-mount-encrypted-partitions-at-boot/
```