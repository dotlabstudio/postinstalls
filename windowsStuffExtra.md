# Get Windows Product Key on an Installed System 

Click on the Start and search for Command Prompt (CMD). 

Or, 

you can also press Windows Key + S to directly open Windows Search.

Open the Command Prompt and enter the below command and then hit the Enter key to view your Windows product key.

```
wmic path SoftwareLicensingService get OA3xOriginalProductKey
```

source: 
```
https://techwiser.com/how-to-find-your-windows-10-11-product-key/
```

# Clean Windows Install

Get fresh available iso 

Acess the bios 

On Boot up, select

**Time and current format English (world)**

Select windows 11 pro

Setup for work or school 

Signin options

Domain join 

Blank password 

**Never Ever Use Reset Windows**

Setup local admin account

Setup a standard user account 

# Turn off internet for Windows 11 install

At the keyboard section 

Shift + F10 keyboard shortcut

In Command Prompt, type the 

```
OOBE\BYPASSNRO
```

command to bypass network requirements on Windows 11 and press Enter.

source:

https://pureinfotech.com/bypass-internet-connection-install-windows-11/


## Show file extensions in file explorer

To view file extensions on Windows 10: 

launch File Explorer, then click on the "View" tab and check the "File Name Extensions" box. 

To show file extensions on Windows 11:

open File Explorer, and then click View > Show > File Name Extensions.

## More file explorer stuff in Windows 11

view - details 

preview pane

folder options

view

check display the full path in the title bar

single-click 

privacy

uncheck all


uncheck

Hide extensions

Click on network 

and turn on network 


## Get Windows Classic Right Click Context Menu in Windows 11 Per User

Edit Context Menu Using Command Prompt

Press the Windows key on your keyboard. Then, type Command Prompt.

Now, type or copy-paste the following command and hit Enter:

```
reg add "HKCU\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\InprocServer32" /f /ve
```


sources: 

https://answers.microsoft.com/en-us/windows/forum/all/restore-old-right-click-context-menu-in-windows-11/a62e797c-eaf3-411b-aeec-e460e6e5a82a


https://www.guidingtech.com/how-to-customize-right-click-menu-windows-11/

reboot

To restore windows 11 context menu back

```
reg.exe delete "HKCU\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}" /f
```

reboot

source: 

https://pureinfotech.com/bring-back-classic-context-menu-windows-11/

# Turn off fast startup

Completely useless feature of windows and it disables the numerical keypad

Open the Control Panel (icons view), and click/tap on the Power Options icon.

Click/tap on the Choose what the power buttons do link on the left side

Click/tap on the Change settings that are currently unavailable link at the top.

Check (on - default) or uncheck (off) Turn on fast startup for what you want, and click/tap on Save changes.

source:
```
https://www.elevenforum.com/t/turn-on-or-off-fast-startup-in-windows-11.1212/
```

or use the CMD

Press the Windows key, type cmd, and select Run as administrator under Command Prompt.

Now, type the command below and hit Enter:

```
Powercfg -h off
```

source:
```
https://windowsreport.com/disable-fast-startup-windows-11/
```

### Windows 11 settings

Turn on Do not disturb mode

Check windows security

settings ---> personalization ---> themes

desktop icon settings

settings ---> personalization ---> start

layout  --> select more pins

uncheck all except for show recently added apps 

settings ---> personalization ---> taskbar

search --> chose search icon only

turn off

- copilot
- widgets

settings ---> personalization ---> accessibility ---> captions

settings ---> personalization ---> privacy & security

nightmare of a settings page to navigate

### Turn off UAC

### Tune Windows 11 for performance

To disable visual effects in Windows, first type performance in the Search box, then select Adjust the appearance and performance of Windows from the list of results.

On the Visual Effects tab, select Adjust for best performance > Apply.

## Make sure the defragmenter schedule is turn off on ssd and enable ssd trim 

The command to test for TRIM is:
```
fsutil behavior query DisableDeleteNotify 
```   

if the result is zero, trim is enabled.

The command to enable TRIM is:
```
fsutil behavior set DisableDeleteNotify 0 
```

## Disable Advertising ID if Edge is not installed 


Disable Service CPDSVC

## Disable Useless Services 

```
Dism /Online /Disable-Feature /FeatureName:"Internet-Explorer-Optional-amd64"
Dism /Online /Disable-Feature /FeatureName:"Remote-Differencial-Compression-API-Support"
Dism /Online /Disable-Feature /FeatureName:"SMB1Protocol"
```

source:

https://github.com/Tech1Guy1One/HomeBrewSecurityFixes/tree/HomeBrew



## Unregister dns services on network cards 

go to

Control panel ---> Network and internet ---> Network Connections

right click on network adapters

select on all the Internet Protocol

click on Properties

Advanced on General tab

click on DNS tab

uncheck

Register this connection's addresses in DNS

Repeat for all adaptors 

## Disable all startup apps

Disable all startup apps including edge 

##  Windows Firewalls

Reset the firewall via cmd:
```
netsh advfirewall set allprofile state on
netsh advfirewall reset
```

Disable all the firewall rules or block everything using power shell:
```
set-netfirewallrule -name "*" -Enabled False
```

Turn on firewal:
```
netsh advfirewall set allprofile state on
```

On firewall profiles 

Block inbound connections 

Allow outgoing connections 

But customise to allow all notifications 


## Secure default accounts 

disable passsowrds never expired and disable them 

## Configure defender 


Disable automatic submission


Exclude windows defender folder in defender 


### SSH server for Windows

https://www.bitvise.com/download-area

https://www.putty.org/

# Get snappy drivers


https://www.glenn.delahoy.com/snappy-driver-installer-origin/


# Windows Utility

Get The Ultimate Windows Utility

```
iwr -useb https://christitus.com/win | iex
```

from 

https://christitus.com/windows-tool/

Just use to Set Services to Manual under Tweaks.


# Get chocolatey

Check First:

With PowerShell, you must ensure Get-ExecutionPolicy is not Restricted. We suggest using Bypass to bypass the policy to get things installed or AllSigned for quite a bit more security.

Run Get-ExecutionPolicy. If it returns Restricted, then run Set-ExecutionPolicy AllSigned or Set-ExecutionPolicy Bypass -Scope Process.

run this command first:
```
Set-ExecutionPolicy AllSigned
```


then run 

```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

from https://chocolatey.org/


## Upgrading Chocolatey

Once installed, Chocolatey can be upgraded in exactly the same way as any other package that has been installed using Chocolatey. Simply use the command to upgrade to the latest stable release of Chocolatey:

```
choco upgrade chocolatey
```

## Upgrade everything in Chocolatey

```
choco upgrade all -y
```

### Some good choco apps

```
choco install Firefox -y

choco install vlc -y

choco install IrfanView irfanviewplugins irfanview-shellextension -y

choco install okular -y

choco install kate -y 

choco install localsend -y


choco install notepadplusplus -y

choco install sumatrapdf -y

choco install git -y

choco install PandaFreeAntivirus -y 
# checksums are not right as 2024-04-10

choco install winscp -y

choco install putty -y

choco install filezilla -y

choco install vim -y

choco install neovim -y
```

For my windows testing rigs:
```
choco install Firefox vlc IrfanView irfanviewplugins irfanview-shellextension okular kate filezilla xournalplusplus libreoffice-fresh PSWindowsUpdate powershell winget powertoys -y
```

Choco List for other family members:
```
choco install Firefox vlc IrfanView irfanviewplugins irfanview-shellextension okular kate xournalplusplus libreoffice-fresh GoogleChrome -y
```


## Windows Anti-virus that can still work under Windows XP and 7 and beyond without an account


Panda Free Antivirus

```
https://www.pandasecurity.com/en/homeusers/free-antivirus/
```


# Portable Apps 
```
https://portableapps.com/
```

## Make portable apps on Windows

Enigma Virtual Box is perfect for creating portable applications

```
https://enigmaprotector.com/en/aboutvb.html
```

```
https://enigmaprotector.com/
```

## Update with Winget  

update all:
```
winget upgrade --include-unknown --all
```

or 
```
winget upgrade --include-unknown --all --accept-package-agreements
```

sources:
```
https://www.xda-developers.com/how-use-winget-windows-11/

https://winget.run/

https://blog.danskingdom.com/Use-WinGet-to-install-and-update-your-Windows-apps/

https://learn.microsoft.com/en-us/windows/package-manager/winget/upgrade
```

### Fix Corrupted Windows Updates

Sometimes Windows Update files get corrupted and the user is not able to download the files again or install the corrupted update files. In that case, we need to run a DISM command to fix the corrupted Windows Update. Here are the steps:

Launch the Command Prompt.

Run the following cmd:

```
dism /Online /Cleanup-image /Restorehealth
```

After successfully running this command, try force downloading the updates again, and the Windows Update should start working again.

Hopefully, this will be useful in situations where you want to automate certain Windows functions. 

You can use these commands to fix things:
```
DISM /ONLINE /CLEANUP-IMAGE /SCANHEALTH 

DISM /ONLINE /CLEANUP-IMAGE /CHECKHEALTH

DISM /ONLINE /CLEANUP-IMAGE /RESTOREHEALTH

Sfc /Scannow 
```

## Windows shortcuts

Quick access:

Super+X

Run:

Super+R 



## Some Useful Windows commands

```
systeminfo 
```

```
netstat -aon
```

Low pid numbers after registry might indicate malware 

```
tasklist /svc | find /i “pidnumber”
```

poweroff the machine:
```
shutdown /p 
```

reboot into firmware bios:
```
shutdown /r /fw /f /t 0
```

## Creating Local User Accounts under Windows 

Show all accounts on the system:
```
net user
```

Create an account with a password:
```
net user Username Password /add
```
Replace Username and Password in the above command with the credentials you want to use for the local account.

Hide password when creating a new user

If you do not want the password to be visible while adding new user account, you can use ‘*’ as shown below.
```
net user /add Username *
```

Create an account without a password:
```
net user Username /add
```

To add a new user account to the domain:
```
net user Username Password /ADD /DOMAIN
```

Add user to the admin group:
```
net localgroup Administrator Username /add
```
Make sure to change Username to the actual name of the account that you want to give administrator privileges.

Change a user password:
```
net user Username NEWPASS
```
To change a password, type the net user command, replacing Username and NEWPASS with the actual username and new password for the account. If the username is more than one word, you'll need to place it inside quotes like below

```
net user "User Name" NEWPASS
```

or 

```
net user Username *
```
You'll need to replace Username with the name of the account for which you wish to change the password. Type the account name exactly as it appears in the account name section of the Command Prompt.

then type:
```
password
```

Rename a local user account:
```
wmic useraccount where name='currentname' rename newname
```

for example:

wmic useraccount where name='xpuser' rename win7user



Use or run the User Accounts under Control Panel:
```
netplwiz
```

Use or run the Local Users and Groups utility:
```
lusrmgr.msc
```

sources:
```
https://www.guidingtech.com/top-ways-to-create-a-local-user-account-in-windows-11/

https://nerdschalk.com/how-to-create-a-new-user-on-windows-11-local-or-online/

https://www.lifewire.com/net-user-command-2618097

https://www.makeuseof.com/tag/quick-tip-change-the-windows-user-password-via-command-line/

https://www.wikihow.com/Change-a-Computer-Password-Using-Command-Prompt

https://www.techbout.com/change-windows-10-password-using-command-prompt-64745/

https://blog.techinline.com/2018/12/20/how-to-change-windows-password-using-command-line-or-powershell/

https://www.windows-commandline.com/add-user-from-command-line/
```


## Windows as Guest Virtual Machine

### Make a fast Windows VM 

https://sysguides.com/install-a-windows-11-virtual-machine-on-kvm


## Share Folder Between Windows Guest and Linux Host in KVM using virtiofs

https://sysguides.com/share-files-between-the-kvm-host-and-windows-guest-using-virtiofs

https://www.debugpoint.com/kvm-share-folder-windows-guest/


in NixOS:

just need to add

```
<binary path='/run/current-system/sw/bin/virtiofsd'/>
```

below driver section in xml file for this module 


### Use USB for the virtual machine 
Show Virtual Hardware Details

click Add Hardware ---> USB Host Device

select your USB device

then click Finish

## Upgrading Windows to Windows 11 on Unsupported Hardware 

Get a fresh windows 11 iso 

Mount it and copy the contents to a folder

Then run:

```
C:\Win11\setup.exe /product server
```

source: 

https://memstechtips.blogspot.com/2023/11/how-to-use-cmd-to-upgrade-to-windows-11-23h2-on-unsupported-hardware.html

## Windows 11 iso on unsupported hardware

Shift + F10 when it borks itself like normal

```
reg add HKLM\System\Setup\LabConfig /v BypassTPMCheck /t reg_dword /d 1

reg add HKLM\System\Setup\LabConfig /v BypassSecureBootCheck /t reg_dword /d 1

reg add HKLM\System\Setup\LabConfig /v BypassRAMCheck /t reg_dword /d 1

reg add HKLM\System\Setup\LabConfig /v BypassCPUCheck /t reg_dword /d 1
```

or use Rufus 

Alt-E - Enable dual BIOS+UEFI mode for Windows installation media. 

or

Alt+Shift+E


sources:

https://www.digitalcitizen.life/install-windows-11-unsupported-hardware/

https://rufus.ie/en/

https://github.com/pbatard/rufus/wiki/FAQ#user-content-Power_keysCheat_modes

## Change Time Zones under Windows via CMD or Powershell

### Change time zone via cmd:

Get the current time zone identifier (TimeZoneID):
```
tzutil /g
```

List all time zones with their names and settings:
```
tzutil /l
```

If you want to quickly find all and display all time zones with a specific offset (for example, with an UTC +10 offset), run the command:
```
tzutil /l | find /I "utc+10"
```

Change the current time zone to (UTC+10:00) AUS Eastern Standard Time (Australia):
```
tzutil /s "AUS Eastern Standard Time"
```

### Set Time Zone via Powershell:

To check the current Windows time zone using PowerShell, run the command:
```
Get-TimeZone
```

List available time zones:
```
Get-TimeZone -ListAvailable
```

You can use filter to search through the list of time zones:
```
Get-TimeZone -ListAvailable | Where-Object {$_.displayname -like "*aus*"}
```

Set a new time zone on Windows:
```
Set-TimeZone -Name "AUS Eastern Standard Time"
```
or
```
GetTimeZone -ListAvailable|? DisplayName -like "*AUS*"|Set-TimeZone
```


Use or run:
```
ms-settings:dateandtime
```

Use or run:
```
intl.cpl
```

Registry keys for "Set time automatically" and "Set time zone automatically" in Settings > Time and Language 
```
[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\W32Time\Parameters](time)
"Type"="NoSync" (off)
"Type"="NTP"    (on)


[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\tzautoupdate](time zone)
"Start"=dword:00000003 (on)
"Start"=dword:00000004 (off)
```

sources: 

https://woshub.com/how-to-set-timezone-from-command-prompt-in-windows/

https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/default-time-zones?view=windows-11

https://superuser.com/questions/1070290/registry-keys-for-automatic-time-and-time-zones


## Change regional settings via powershell

Display the locale information:
```
Get-WinSystemLocale
```

Set the locale for the region and language:
```
Set-WinSystemLocale en-AU
```

List home locatiion 
```
Get-WinHomeLocation
```

leave home location as the world or set as the world:
```
Set-WinHomeLocation -GeoID 39070
```

Set home regional settings to Australian:
```
Set-WinHomeLocation -GeoID 12
```

List regional formats:
```
Get-Culture
```

Change regional formats to Australian:
```
Set-Culture en-AU
```


sources:

https://pureinfotech.com/change-region-settings-windows-10/

https://learn.microsoft.com/en-us/windows/win32/intl/table-of-geographical-locations

https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/configure-international-settings-in-windows?view=windows-11


## Change computer name via cmd or powershell


List the current computer name:
```
hostname
```

Change computer name in cmd:
```
WMIC computersystem where caption='CurrentName' rename NewName
```
Replace CurrentName with the existing name of the computer, and NewName with the new name you wish to assign to the computer


Change the computer name and reboot in powershell:
```
Rename-Computer -NewName <ComputerName> -Restart
```

for exampe:

Rename-Computer -NewName Win10 -Restart


Use or run:
```
sysdm.cpl
```

sources:
```
https://www.itechtics.com/change-computer-name/

https://www.ionos.com/digitalguide/server/configuration/change-computer-name/

https://www.digitalcitizen.life/change-computer-name-windows/

https://www.shellhacks.com/change-computer-name-windows-cmd-powershell/
```
## File Sharing under Windows via CMD

Turn on the file sharing:
```
netsh advfirewall firewall set rule group="File and Printer Sharing" new enable=Yes
netsh advfirewall firewall set rule group="Network Discovery" new enable=Yes
```

List whats shared currently on your system:
```
net share
```

To share a folder:
```
net share NetworkFiles=C:\SharedFolder /grant:everyone,full
```
The NetworkFiles is the share name of SharedFolder which previously created with command.
The /grant:everyone,full assign shared permission to everyone’s group. You can assign /change or /read permission.

To see the shared folders of Windows 10
```
net view \\Win10
```

let’s connect to shared folders of Windows 10 from another Windows box:
```
net use Z: \\Win10\NetworkFiles
```
and create a network map drive for NetworkFiles folder.

Remove shared folder once no longer needed:
```
net share /delete mysharedfolder
```

Use or run:
```
fsmgmt.msc
```

sources:

https://techviewleo.com/install-and-configure-samba-share-on-windows/

https://www.technig.com/share-files-using-command-line/

https://www.simplified.guide/microsoft-windows/share-folder-from-a-command-line

## Backup and Restore Windows Drivers via CMD

Create a folder for the drivers first

Backup Windows drivers:
```
dism /online /export-driver /destination:c:\drivers
```

or 

```
pnputil /export-driver * "c:\drivers"​
```

Restore Windows drivers:
```
dism /online /add-driver /driver:c:\drivers /recurse
```

or
```
pnputil /add-driver "c:\drivers\*.inf" /subdirs /install /reboot
```


gui for backuping and restoring windows drivers:
```
https://github.com/lostindark/DriverStoreExplorer
```

sources:
```

https://www.elevenforum.com/t/backup-and-restore-device-drivers-in-windows-11.8678/

https://www.technorms.com/87156/how-to-backup-drivers

https://bytebitebit.com/tips-tricks/windows/backup-and-restore-device-drivers-in-windows-10/

https://www.howtoedge.com/backup-and-restore-drivers-using-command-prompt/

https://www.tenforums.com/tutorials/68426-backup-restore-device-drivers-windows-10-a.html
```


## Disable NVIDIA Control Panel not found popups

NVidia is just as annoying in Windows as in Linux  

Run:
```
services.msc
```

Locate ‘NVIDIA Display Container LS’

Right-click on it and choose ‘Properties’

Change ‘Startup type’ to ‘Disabled


source:
```
https://www.itsupportguides.com/knowledge-base/windows-10windows-10-how-to-remove-nvidia-control-panel-is-not-found-prompt/
```

# Auto Install ISO for Windows with An Answer xml file

Get the official windows iso from :

```
https://www.microsoft.com/en-us/software-download/windows11
```

Get a preloaded answer file from:

```
https://github.com/memstechtips/UnattendedWinstall
```

or generate an xml answer file from:
```
https://schneegans.de/windows/unattend-generator/
```

Create a custom iso with the answer file using k3b under Linux or 

Anyburn under windows.
```
https://anyburn.com/download.php
```

Or use ventoy with the Ventoy Auto Install plugin and VentoyPlugson.






