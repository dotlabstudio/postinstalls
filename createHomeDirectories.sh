#!/usr/bin/env bash

echo " This script just create home directories for a fresh system"

cd ~

mkdir -p Documents

mkdir -p Downloads

mkdir -p Music

mkdir -p Pictures

mkdir -p Public

mkdir -p Videos/Jellyfin

mkdir -p Videos/videosDone

mkdir -p Videos/srt 

mkdir -p Videos/tmp 

mkdir -p lab

mkdir -p ventoyBurned

mkdir -p ventoyISOs

mkdir -p zArchived

mkdir -p zOffline

mkdir -p .local/share/fonts/

echo "Finished setting home directories"
echo "Please copy download script to Downloads and Videos folders"
