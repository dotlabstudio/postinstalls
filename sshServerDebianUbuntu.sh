#!/usr/bin/env bash

echo "this script setup a ssh server on Debian or Ubuntu system"

# install ssh server
sudo apt install openssh-server -y

# check status of ssh server
# sudo systemctl status ssh

# start ssh server
#sudo systemctl start sshd

# enable ssh server at boot
sudo systemctl enable --now ssh

echo "update ssh server configs and copy over with my configs"

sudo cp hardenSSH.conf /etc/ssh/sshd_config.d/hardenSSH.conf

sudo cp issue.net /etc/issue.net
#sudo cp issue /etc/issue

echo "create a ssh group and add primary user to the group"
sudo groupadd sshusers
sudo usermod -a -G sshusers $USER


echo "Finished setting up ssh server for Debian or Ubuntu"