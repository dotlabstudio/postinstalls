#!/usr/bin/env bash

echo "remove the stable repos first"
echo " "
echo "Do not mix development and stable repositories! Be sure to have installed in the system or the stable or the development repositories only."

sudo luet uninstall -y repository/mocaccino-extra-stable repository/mocaccino-desktop-stable repository/mocaccino-os-commons-stable repository/mocaccino-community-stable

echo " "
echo "add the development repos"

sudo luet install -y --nodeps repository/mocaccino-extra repository/mocaccino-kernel repository/mocaccino-desktop repository/mocaccino-os-commons repository/mocaccino-community

echo " "
echo "refresh the repos"
sudo luet repo update

#echo " "
#echo "install the fish shell"
#sudo luet install -y apps/fish
