# Disable The Shutdown Event For Windows Servers or Suppressing the "reason" for shutdown on Windows Server

Open "Group Policy Management Console" 

can be opened via Win+R and then executing 
```
gpedit.msc
```

navigate to Computer Configuration >> Administrative Templates >> System 

search for 
```
Display Shutdown Event Tracker
```

in All Settings

Then Disable It.

The above works well.

or command line (not tested):
```
reg.exe add "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Reliability" /v ShutDownReasonOn /t REG_DWORD /d 0 /f
```
