## Creating Local User Accounts under Windows 

Show all accounts on the system:
```
net user
```

Create an account with a password:
```
net user Username Password /add
```
Replace Username and Password in the above command with the credentials you want to use for the local account.

Hide password when creating a new user

If you do not want the password to be visible while adding new user account, you can use ‘*’ as shown below.
```
net user /add Username *
```

Create an account without a password:
```
net user Username /add
```

To add a new user account to the domain:
```
net user Username Password /ADD /DOMAIN
```

Add user to the admin group:
```
net localgroup Administrator Username /add
```
Make sure to change Username to the actual name of the account that you want to give administrator privileges.

Change a user password:
```
net user Username NEWPASS
```
To change a password, type the net user command, replacing Username and NEWPASS with the actual username and new password for the account. If the username is more than one word, you'll need to place it inside quotes like below

```
net user "User Name" NEWPASS
```

or 

```
net user Username *
```
You'll need to replace Username with the name of the account for which you wish to change the password. Type the account name exactly as it appears in the account name section of the Command Prompt.

then type:
```
password
```

Rename a local user account:
```
wmic useraccount where name='currentname' rename newname
```

for example:

wmic useraccount where name='xpuser' rename win7user



Use or run the User Accounts under Control Panel:
```
netplwiz
```

Use or run the Local Users and Groups utility:
```
lusrmgr.msc
```