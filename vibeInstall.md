# Install Vibe on Linux

## Quick Install
```
curl -sSf https://thewh1teagle.github.io/vibe/installer.sh | sh -s v3.0.0
```

## Arch 

Yay (Arch Linux)
```
yay -S vibe-bin
```

## Cachyos
```
paru -Syyu vibe-bin
```

source:
```
https://thewh1teagle.github.io/vibe/
```