Fonts under Linux
Install or copy fonts to:

~/.local/share/fonts/


create the fonts folder:

mkdir -p ~/.local/share/fonts/


Finally, update the fontconfig cache (usually unnecessary as software using the fontconfig library does this):

fc-cache