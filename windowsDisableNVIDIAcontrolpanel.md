## Disable NVIDIA Control Panel not found popups

NVidia is just as annoying in Windows as in Linux  

Run:
```
services.msc
```

Locate ‘NVIDIA Display Container LS’

Right-click on it and choose ‘Properties’

Change ‘Startup type’ to ‘Disabled