Server Manager Properties: 

Open Server Manager,

click Manage, and then Server Manager Properties. 

Uncheck the box next to Do not start Server Manager automatically at logon and click OK.

Command Line: Use the following command to disable Server Manager from starting at login:
```
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\ServerManager" /v StartAtLogon /t REG_DWORD /d 0 /f
```