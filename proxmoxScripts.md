# Proxmox VE Helper-Scripts

website:
```
https://tteck.github.io/Proxmox/
```

github:
```
https://github.com/tteck/Proxmox
```

Scripts must be run in the PVE shell

Proxmox VE Post Install:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/misc/post-pve-install.sh)"
```

yes to the defaults is good.

Docker LXC:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/ct/docker.sh)"
```

Debian LXC:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/ct/debian.sh)"
```

iVentoy LXC:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/ct/iventoy.sh)"
```

container must be Privileged.


Tailscale for existing LXC containers:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/misc/add-tailscale-lxc.sh)"
```

After the script finishes, reboot the LXC then run tailscale up in the LXC console

Nextcloud LXC:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/ct/nextcloudpi.sh)"
```


These scripts should be run inside the LXC containers

CrowdSec:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/misc/crowdsec.sh)"
```

