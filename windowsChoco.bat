choco install Firefox vlc IrfanView irfanviewplugins irfanview-shellextension okular kate filezilla xournalplusplus libreoffice-fresh -y

# old stuff
# choco install PSWindowsUpdate powershell winget powertoys -y

# for windows to go stuff
choco install ventoy rufus -y

# rufus website
# https://rufus.ie/en/

# for windows gui package manager 
# https://github.com/marticliment/UniGetUI/
#
# https://www.marticliment.com/unigetui/
choco install wingetui -y

# Programs under
# ProgramData\chocolatey\lib\