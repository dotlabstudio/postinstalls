# Windows gui package manager - UniGetUI (formerly WingetUI)

# github page 
```
https://github.com/marticliment/UniGetUI/
```

# The OFFICIAL website for UniGetUI is 
```
https://www.marticliment.com/unigetui/
```

# install from chocolatey
choco install wingetui -y
