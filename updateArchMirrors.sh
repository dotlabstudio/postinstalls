#!/usr/bin/env bash

sudo pacman -Sy rate-mirrors

rate-mirrors arch --max-delay 7200 | sudo tee /etc/pacman.d/mirrorlist

sudo pacman -Syyu

