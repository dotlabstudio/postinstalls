#!/usr/bin/env bash

echo "this script install the virtual machine tools for proxmox or kvm or qemu for Arch Based virtual machines"
echo " "

echo " "
echo "update system first"
paru -Syyu --noconfirm

echo " "
echo "Enable trim operations on SSD/NVME:"
sudo systemctl enable --now fstrim.timer

echo "guest tools for proxmox or kvm or qemu"
paru -Syyu --noconfirm --needed qemu-guest-agent spice-vdagent

echo " "
echo "install my cli tools"
paru -Syyu --noconfirm --needed lynis sysstat rkhunter arch-audit
paru -Syyu --noconfirm --needed cowfortune clamav htop ouch yt-dlp tealdeer

echo " "
echo "install spellcheckers"
paru -Syyu --noconfirm --needed aspell aspell-en hunspell-en_au hunspell-en_gb

echo " "
echo " update databases"
sudo updatedb
tldr --update 

echo " "
echo "update clamav"
sudo freshclam

#enable ssh server or disable sshd server
echo " "
echo "setup ssh server"
echo " with ssh server enabled - lynis score is 71"
sudo systemctl start sshd && sudo systemctl enable sshd

#echo "disable ssh server"
echo " "
echo " with ssh server disable - lynis score is 66"
# sudo systemctl disable sshd

echo " "
echo " copy harden ssh setup"
sudo cp issue.net /etc/issue.net
#sudo cp hardenSSH.conf /etc/ssh/sshd_config.d/

echo " "
echo "setup tailscale"
paru -Syyu --noconfirm --needed tailscale

sudo systemctl enable --now tailscaled

echo "virtual machines guest tools are installed - please reboot"