#!/usr/bin/env bash

echo "This script will setup the firewall rules after installing gufw or ufw on a fresh system that has no firewall preinstalled"

sudo ufw default allow outgoing comment 'allow all outgoing traffic'
sudo ufw default deny incoming comment 'deny all incoming traffic'

sudo ufw allow ssh

sudo ufw limit in ssh comment 'allow SSH connections in'

# allow traffic out to port 53 -- DNS
sudo ufw allow out 53 comment 'allow DNS calls out'

# allow traffic out to port 123 -- NTP
sudo ufw allow out 123 comment 'allow NTP out'

# allow traffic out for HTTP, HTTPS, or FTP
# apt might needs these depending on which sources you're using
sudo ufw allow out http comment 'allow HTTP traffic out'
sudo ufw allow out https comment 'allow HTTPS traffic out'
sudo ufw allow out ftp comment 'allow FTP traffic out'

# allow whois
#sudo ufw allow out whois comment 'allow whois'
# no profile exist for whois now

# allow traffic out to port 68 -- the DHCP client
# you only need this if you're using DHCP
sudo ufw allow out 67 comment 'allow the DHCP client to update'
sudo ufw allow out 68 comment 'allow the DHCP client to update'

sudo ufw enable

echo "Finsihed setting up firewall for new system. Please reboot"
