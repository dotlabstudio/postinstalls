#!/usr/bin/env bash

echo " "
echo "setup virtualbox for a Archy based distro with paru like cachyos"
paru -Syyu --noconfirm --needed linux-headers virtualbox virtualbox-guest-iso virtualbox-ext-oracle net-tools
# no longer using virtualbox

echo " "
echo "add user to vboxusers"
sudo gpasswd -a $USER vboxusers
