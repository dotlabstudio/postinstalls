source: https://en.opensuse.org/VirtualBox

sudo transactional-update pkg install virtualbox virtualbox-guest-tools virtualbox-qt virtualbox-host-source kernel-devel kernel-default-devel

Need to adduser to vbox group

sudo usermod -a -G vboxusers $USER

To check
Groups $USER

Also add vboxsf group
