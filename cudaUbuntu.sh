#!/usr/bin/env bash

# This script installs cuda stuff for my ubuntu distrobox for vibe
# source: 
# https://developer.nvidia.com/cuda-downloads


echo " install Nvidia cuda toolkit from the native repos"
sudo apt install -y nvidia-cuda-toolkit

echo " Download Base Installer for Linux Ubuntu 24.04 x86_64 "

wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2404/x86_64/cuda-keyring_1.1-1_all.deb

sudo dpkg -i cuda-keyring_1.1-1_all.deb
sudo apt update
sudo apt -y install cuda-toolkit

echo " NVIDIA Driver "
sudo apt install -y nvidia-open

echo "finished setting cuda for Ubuntu distrobox"