# Windows To Go

## Backup windows drivers with 
```
dism /online /export-driver /destination:c:\drivers
```

Plug your USB drive/installation media into your computer.

Create a folder named 
```
$WinpeDriver$
```
on your USB drive.

Copy the drivers you want to install automatically from C:\Drivers to D:\$WinpeDriver$

assuming D: is your USB drive.


## Get normal windows or LTSC iso - server iso do not work properly with windows to Go

## Get latest rufus

### in rufus

Select the windows iso

Select Windows To Go for installation

Use MBR for both bios and uefi

change volume label to
```
Local Disk
```

Use default filesystem of NTFS


uncheck created extended labels and icons

uncheck Prevent acccessing internal drives 

# Copy windows driver to the new local disk 

# Restore Windows drivers 
```
pnputil /add-driver "c:\drivers\*.inf" /subdirs /install /reboot
```

# Disable password expiry for user accounts
Run or Win + Run
```
lusrmgr.msc
```

# Disable Fast Boot
```
Powercfg -h off
```

# Get and Install UniGetUI (formerly WingetUI)

The Graphical Interface for your package managers
```
https://www.marticliment.com/unigetui/
```
# Chocolatey Software is still the Best
```
https://chocolatey.org/install
```

install chocolatey:
```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

# Choco Apps
```
choco install Firefox vlc IrfanView irfanviewplugins irfanview-shellextension okular kate filezilla xournalplusplus libreoffice-fresh -y
```

Choco List for other family members:
```
choco install Firefox vlc IrfanView irfanviewplugins irfanview-shellextension okular kate xournalplusplus libreoffice-fresh GoogleChrome -y
```

# Activate Windows
```
irm https://get.activated.win | iex
```

# Windows 11 fixes 

## right click menu
```
reg add "HKCU\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\InprocServer32" /f /ve
```

## Time zone
```
tzutil /s "AUS Eastern Standard Time"
```

## Regional Settings
```
Set-Culture en-AU

Set-WinSystemLocale en-AU
```

## Turn off Windows Recall
On the command line, check for it:
```
dism /online /Get-Featureinfo /FeatureName:Recall
```

disable recall:
```
dism /online /Disable-Feature /FeatureName:Recall
```

Under Group policy:

Check for Administrative templates

Search for 

Windows ai


Enable 
```
turn off saving snapshots for windows
```

# To Restore the Windows Store App on LTSC Windows version

Microsoft Store

LTSC editions do not come with store apps pre-installed. To install them, follow the steps below.

Make sure the Internet is connected.
Open Powershell as admin and enter,
```
wsreset -i
```

Wait for a notification to appear that the store app is installed, it may take a few minutes.

On Windows 10 2021 LTSC, you might encounter an error indicating that cliprenew.exe cannot be found. This error can be safely ignored.

App Installer

This app is very useful; it includes WinGet, enabling easy installation of .appx packages. After installing the Store app, install the App installer from this URL.
```
https://apps.microsoft.com/detail/9nblggh4nns1
```
