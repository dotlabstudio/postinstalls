#!/usr/bin/env bash

echo "This preps an Ubuntu Server for the hardening script"
echo "don't install the ssh server - the script will install it"

sudo apt-get -y install git net-tools procps --no-install-recommends

cd ~
git clone https://github.com/konstruktoid/hardening.git

echo " "
echo "Change the configuration options in the ubuntu.cfg file."
echo " Make sure to update the CHANGEME variable otherwise the script will fail."
echo " "
echo "then run"
echo "sudo bash ubuntu.sh"