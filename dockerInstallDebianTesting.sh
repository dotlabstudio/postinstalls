#!/usr/bin/env bash

echo "this script setups docker for a Debian testing system"

echo " "
echo "update system first"
sudo apt-get update && sudo apt-get fullgrade -y

echo " "
echo " install dcoker stuff in the standard native repo "
sudo apt-get install docker.io docker-cli podman-compose -y

# create the docker group and add your user:

echo "Create the docker group"
sudo groupadd docker

echo "Add your user to the docker group"
sudo usermod -aG docker $USER

echo "activate new group"
newgrp docker

echo "Log out and log back in so that your group membership is re-evaluated"