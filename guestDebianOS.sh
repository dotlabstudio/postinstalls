#!/usr/bin/env bash

echo " This script just installs the guest os tools under virt manager for a Debian vm"

sudo apt-get install spice-vdagent spice-webdavd qemu-guest-agent avahi-autoipd avahi-autoipd -y

# remove virtiofsd from the list as ubuntu does not have it in the repos but debian does
# sudo apt-get install virtiofsd -y

sudo apt-get install gir1.2-spiceclientgtk-3.0 spice-client-gtk -y

sudo apt-get install unattended-upgrades -y

echo " install my cli tools for debian "

sudo apt-get install fish wget curl git fortune neofetch vim -y

echo "Finished setting Debian vm"
