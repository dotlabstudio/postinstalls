#!/usr/bin/env bash

echo "This is a post script for virt-manager"

# check virtual machine services are running
#sudo systemctl status libvirtd.service

# Start Default Network for Networking for virtual machines
sudo virsh net-start default
sudo virsh net-autostart default

# check virtual machine networks are running
sudo virsh net-list --all

# add user to the virtual machine groups
sudo usermod -aG libvirt $USER
sudo usermod -aG libvirt-qemu $USER
sudo usermod -aG kvm $USER
sudo usermod -aG input $USER
sudo usermod -aG disk $USER

echo "Finished setting up virt manager for new system. Please reboot"
