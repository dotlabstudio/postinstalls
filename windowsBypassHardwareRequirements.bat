reg add HKLM\System\Setup\LabConfig /v BypassTPMCheck /t reg_dword /d 1

reg add HKLM\System\Setup\LabConfig /v BypassSecureBootCheck /t reg_dword /d 1

reg add HKLM\System\Setup\LabConfig /v BypassRAMCheck /t reg_dword /d 1

reg add HKLM\System\Setup\LabConfig /v BypassCPUCheck /t reg_dword /d 1
