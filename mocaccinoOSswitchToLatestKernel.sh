#!/usr/bin/env bash

echo "list the kernels"
echo "please wait"
sudo mos kernel-switcher list

echo "switch to the latest mainline kernel"

sudo mos kernel-switcher switch kernel/mocaccino-full
