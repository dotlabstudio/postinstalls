#!/usr/bin/env bash

# this script automates my test run for a new configuration.nix

rm -rf postinstalls

echo "git clone updated post install repo"
echo " "

git clone https://gitlab.com/dotlabstudio/postinstalls.git

echo "change into the repo and make scripts runable"
cd postinstalls 
chmod +x *.sh

echo "please copy this script to the lab folder"
echo "cp grabUpdatedRepoPost.sh ~/lab"
