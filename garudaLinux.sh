#!/usr/bin/env bash

echo "post install script for garuda linux"

echo "the system already has fish and paru installed"

echo " "
echo "update system first"
paru -Syyu --noconfirm

echo " "
echo "Enable global menu:"
paru -Syyu --noconfirm --needed appmenu-gtk-module libdbusmenu-glib 

echo " "
echo "Enable trim operations on SSD/NVME:"
sudo systemctl enable --now fstrim.timer

echo " "
echo " update databases"
sudo updatedb
tldr --update 

echo " "
echo "install my cli tools"
paru -Syyu --noconfirm --needed lynis sysstat rkhunter arch-audit
paru -Syyu --noconfirm --needed wayland-protocols plasma-wayland-protocols xwaylandvideobridge
paru -Syyu --noconfirm --needed cowfortune clamav htop ouch yt-dlp 

echo " "
echo "install spellcheckers"
paru -Syyu --noconfirm --needed aspell aspell-en hunspell-en_au hunspell-en_gb

echo " "
echo "update clamav"
sudo freshclam

echo " "
echo "install my apps from standard repos "
paru -Syyu --noconfirm --needed gnome-boxes minder marble wesnoth gnome-chess gnuchess knights blender converseen digikam darktable 

paru -Syyu --noconfirm --needed gimagereader-qt gimp inkscape kcolorchooser kolourpaint krita brave-bin filezilla

paru -Syyu --noconfirm --needed monero monero-gui p2pool xmrig qbittorrent handbrake haruna signal-desktop

paru -Syyu --noconfirm --needed k3b dvd+rw-tools cdrtools kamoso kdenlive obs-studio qpwgraph strawberry tenacity 

paru -Syyu --noconfirm --needed libreoffice-fresh pdfarranger xournalpp kcalc kooha localsend nextcloud-client

echo " "
echo "setup tailscale"
paru -Syyu --noconfirm --needed tailscale

sudo systemctl enable --now tailscaled

# echo " "
# echo "setup virtualbox"
# paru -Syyu --noconfirm --needed linux-headers virtualbox virtualbox-guest-iso virtualbox-ext-oracle net-tools
# no longer using virtualbox

# echo " "
#  echo "add user to vboxusers"
# sudo gpasswd -a $USER vboxusers

enable ssh server or disable sshd server
echo " "
#echo "setup ssh server"
#sudo systemctl start sshd && sudo systemctl enable sshd

#echo "disable ssh server"
#sudo systemctl disable sshd
echo " copy harden ssh setup"
sudo cp issue.net /etc/issue.net
sudo cp hardenSSH.conf /etc/ssh/sshd_config.d/

#echo " "
#echo " use the firewall.sh scrpt for ufw rules if enabling a ssh server "
#echo " ufw is already enabled and active on cachyos by default "
#paru -Syyu --noconfirm --needed gufw

echo " "
echo " install microsoft font from cachyos repos"
paru -Syyu --noconfirm --needed ttf-ms-fonts

echo " "
echo "get the kernel headers"
paru -Syyu --noconfirm --needed linux-headers net-tools

echo " "
echo "setup virt manager"
paru -Syyu --noconfirm --needed virt-manager qemu-desktop libvirt edk2-ovmf dnsmasq vde2 bridge-utils iptables-nft dmidecode

paru -Syyu --noconfirm --needed qemu-block-gluster qemu-block-iscsi libguestfs qemu-emulators-full

#paru -Syyu --noconfirm --needed qemu-arch-extra

echo " "
echo "setup virt manager services"
sudo systemctl enable --now libvirtd.service

echo "Ensure firewalld is configured to use the nftables backend:"
sudo firewall-cmd --set-default-zone=public --permanent
sudo systemctl reload firewalld
sudo firewall-cmd --reload
sudo systemctl restart libvirtd


sudo virsh net-autostart default
sudo virsh net-start default

echo " "
echo "add user to libvirt groups "
sudo usermod -aG libvirt $USER
sudo usermod -aG libvirt-qemu $USER
sudo usermod -aG kvm $USER
sudo usermod -aG input $USER
sudo usermod -aG disk $USER

echo " "
echo " append to default libvirt.conf file"
sudo cp /etc/libvirt/libvirtd.conf /etc/libvirt/libvirtd.conf.install 
sudo cp libvirtd.conf /etc/libvirt/libvirtd.conf

echo " "
echo "install my list of apps from aur last"

paru -Syyu --noconfirm --needed tor-browser-browser
#paru -Syyu --noconfirm --needed livecaptions
#paru -Syyu --noconfirm --needed glaxnimate
#paru -Syyu --noconfirm --needed switcheroo-gtk4 
#paru -Syyu --noconfirm --needed lagrange-bin
#paru -Syyu --noconfirm --needed standardnotes-bin
paru -Syyu --noconfirm --needed notesnook
#paru -Syyu --noconfirm --needed cryptomator-bin 
#paru -Syyu --noconfirm --needed exodus
paru -Syyu --noconfirm --needed mullvad-browser-bin


echo " "
echo "finished setting up a fresh garuda linux system"
