#!/usr/bin/env bash

#enable daemons:
echo 'start and enable libvirt daemons'
sudo systemctl enable --now libvirtd

sudo systemctl enable --now libvirt-guests

sudo systemctl start libvirtd

#Define the default (masqueraded to host) network:
echo "start virtual networks and make it permanently by default"
#sudo virsh net-define /etc/libvirt/qemu/networks/default.xml

sudo virsh net-start default

#sudo virsh net-autostart default
sudo virsh net-autostart --network default

# add user to libvirtd
#sudo usermod -a -G libvirt $(whoami)

echo "Add your user to the libvirt group"
sudo usermod -aG libvirt $USER

echo "Add your user to the kvm group"
sudo usermod -aG kvm $USER

echo "Finished initial setup for virt manager, now edit the virt manager network"

# source: https://www.mocaccino.org/docs/desktop/virt-manager/

