#!/usr/bin/env bash

echo "this script sets a tumbleweed server on proxmox vm"

echo " "
echo "update the system first"
sudo zypper ref
sudo zypper --non-interactive dup

echo " "
echo " getthe basic stuff"
sudo zypper --non-interactive install fish fastfetch git

echo "change shell to fish"
chsh -s /usr/bin/fish

echo " "
echo "install spice"
sudo zypper --non-interactive install spice-vdagent spice-webdavd

echo " add user to kvm group"
sudo usermod -aG kvm $USER

echo "install docker"
sudo zypper --non-interactive install docker docker-compose docker-machine-driver-kvm2

echo " add user to docker group"
sudo usermod -aG docker $USER
newgrp docker

echo "start and enable docker"
sudo systemctl start docker
sudo systemctl enable docker

echo " install Avahi"
sudo zypper --non-interactive install avahi avahi-utils

echo " start and enable avahi"
sudo systemctl start avahi-daemon
sudo systemctl enable avahi-daemon

echo "setup automatic updates via transactional updates"
sudo zypper --non-interactive install transactional-update rebootmgr
sudo systemctl enable --now transactional-update.timer transactional-update-cleanup.timer rebootmgr.service 

