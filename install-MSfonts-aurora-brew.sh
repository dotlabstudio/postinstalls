#!/bin/bash

# reference
# https://universal-blue.discourse.group/t/suggestion-ujust-install-microsoft-fonts/6038/3

# Ensure cabextract is installed
if ! command -v cabextract &> /dev/null
then
    echo "cabextract could not be found, installing..."
    brew install -y cabextract
fi

# Create the fonts directory if it doesn't exist
mkdir -p ~/.local/share/fonts

# Navigate to the fonts directory
cd ~/.local/share/fonts

# Array of Microsoft font executables
font_exes=(
    "andale32.exe"
    "arial32.exe"
    "arialb32.exe"
    "comic32.exe"
    "courie32.exe"
    "georgi32.exe"
    "impact32.exe"
    "times32.exe"
    "trebuc32.exe"
    "verdan32.exe"
    "webdin32.exe"
)

# Function to download and extract a font
download_and_extract() {
    exe=$1
    url="https://sourceforge.net/projects/corefonts/files/the%20fonts/final/${exe}/download"
    echo "Downloading $exe from $url"
    curl -sL "$url" -o "$exe"
    if [ $? -ne 0 ]; then
        echo "Failed to download $exe"
        return
    fi
    cabextract -q -F '*.ttf' "$exe"
    if [ $? -ne 0 ]; then
        echo "Failed to extract $exe"
        return
    fi
    rm "$exe"
}

# Download and extract each font concurrently
for exe in "${font_exes[@]}"
do
    download_and_extract "$exe" &
done

# Wait for all background jobs to finish
wait

# Update the font cache
fc-cache -fv

echo "Microsoft fonts have been installed in ~/.local/share/fonts"