#!/usr/bin/env bash

echo " this script will install docker for Arch based systems using paru like cachyos "

# reference: 
# https://linuxiac.com/how-to-install-docker-on-arch-linux/

echo " "

echo " Install Docker and Docker Compose from the official arch repository "
paru -Syyu --noconfirm docker docker-buildx docker-compose

#echo " start the docker service by using systemctl command "
#sudo systemctl start docker.service

#echo " use systemctl command to enable the docker service. Now, Docker will restart whenever the machine boots up."
#sudo systemctl enable docker.service

echo " To ensure Docker starts automatically at boot, enable and start the service "
sudo systemctl enable --now docker.service

echo " use Docker without sudo privileges you will need to add your account to the docker group."
sudo usermod -aG docker $USER
newgrp docker

echo " "
echo " docker is setup for this Arch-based system "