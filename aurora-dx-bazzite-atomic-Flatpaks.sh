#!/usr/bin/bash

echo "this script install system wide flatpaks for aurora-dx or bazzite or atomic systems"

# install flatpaks from flathub
#https://flathub.org/home
#
#flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#  the above one installs flathub repos as a user
#flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

# Development
flatpak install flathub org.gnome.meld -y

# Education
flatpak install flathub org.kde.marble -y

# Games
flatpak install flathub org.wesnoth.Wesnoth -y

flatpak install flathub org.gnome.Chess -y

#flatpak install flathub com.github.sakya.corechess -y
# crashes out on exit or hard to quit

flatpak install flathub org.kde.kapman -y
# Kapman is a clone of the well known game Pac-Man.

flatpak install flathub org.kde.kblocks -y
# KBlocks is the classic falling blocks game

#flatpak install flathub org.kde.kigo -y
# Kigo is an open-source implementation of the popular Go game

#flatpak install flathub org.kde.kmahjongg -y
# mahjongg

flatpak install flathub org.kde.knights -y

#flatpak install flathub org.kde.kolf -y
# Kolf is a miniature golf game

#flatpak install flathub org.kde.kpat -y
# Solitaire card game

#flatpak install flathub org.kde.ksirk -y
# KsirK is a computerized version of the well known strategic board game Risk

flatpak install flathub net.pokerth.PokerTH -y
# not verified

flatpak install flathub com.github.k4zmu2a.spacecadetpinball -y
# not verified
# Game engine for Space Cadet Pinball

#flatpak install flathub lv.martinsz.millionaire -y
# not verified
# Test your knowledge to see if you can answer all questions and win $1 million. Have fun!
# crashes often

# I'm not very good at gaming anymore

# Graphics
flatpak install flathub org.kde.digikam -y

flatpak install flathub org.darktable.Darktable -y
# not verified

flatpak install flathub io.github.manisandro.gImageReader -y
# not verified
# ocr
# needs to be near the top or first because it makes system changes


flatpak install flathub org.gimp.GIMP -y

flatpak install flathub org.inkscape.Inkscape -y

flatpak install flathub org.kde.kcolorchooser -y

flatpak install flathub org.kde.kolourpaint -y

flatpak install flathub org.kde.krita -y

flatpak install flathub com.github.unrud.djpdf -y
# scans to pdf

flatpak install flathub org.kde.skanlite -y
# kde scanning app

flatpak install flathub io.gitlab.adhami3310.Converter -y
# Switcheroo
# convert and manipulate images

# Internet
flatpak install flathub com.brave.Browser -y

flatpak install flathub org.cockpit_project.CockpitClient -y

flatpak install flathub org.filezillaproject.Filezilla -y
# not verified

flatpak install flathub org.garudalinux.firedragon -y

flatpak install flathub fi.skyjake.Lagrange -y

#flatpak install flathub org.getmonero.Monero -y
# not verified
# needs a powerful modern machine with plenty of spare cores and ram and fast ssd
# a beefy machine with plenty of ram and fast storage - 64 GB RAM & 2 TB nvme drive, at least 16 cores

flatpak install flathub net.mullvad.MullvadBrowser -y
# not verified

flatpak install flathub org.nickvision.tubeconverter -y
# Parablic
# another yt-dlp frontend with a name change to parabolic

#flatpak install flathub org.signal.Signal -y
# not verified
# can use signal again with my new iphone

flatpak install flathub com.synology.SynologyDrive -y
# not verified

flatpak install flathub org.torproject.torbrowser-launcher -y
# tor browser has change it's name to

flatpak install flathub io.github.giantpinkrobots.varia -y
# Download files, videos and torrents
# Varia is better than your browser at downloading stuff

# Multimedia
flatpak install flathub org.audacityteam.Audacity -y
# not verified

flatpak install flathub org.gnome.Brasero -y
# not verified
# waiting for k3b flatpak - but this will work in the meantime

#flatpak install flathub io.github.dimtpap.coppwr -y
# Low level control GUI for PipeWire

flatpak install flathub org.freac.freac -y
# Audio converter and CD ripper

flatpak install flathub fr.handbrake.ghb -y

flatpak install flathub org.kde.kamoso -y

flatpak install flathub org.kde.kdenlive -y

#flatpak install flathub com.obsproject.Studio -y
# using old runtimes as 2025-01-19

flatpak install flathub com.makemkv.MakeMKV -y
# not verified
# DVD and Blu-ray to MKV converter and network streamer
# dvd and blu ray ripper
# NOTE: users of this software must accept the terms of the "End User License Agreement".

flatpak install flathub com.poweriso.PowerISO -y
# not verified

#flatpak install flathub org.rncbc.qpwgraph -y
# pipewire graph qt gui interface

flatpak install flathub org.strawberrymusicplayer.strawberry -y

flatpak install flathub org.nickvision.tagger -y

flatpak install flathub org.videolan.VLC -y
# not verified

# Office
flatpak install flathub com.github.tenderowl.frog -y
# ocr
# extract text from any image, video or web page

flatpak install flathub org.libreoffice.LibreOffice -y

flatpak install flathub com.github.dynobo.normcap -y
# ocr
# extract text from an image directly into clipboard

flatpak install flathub com.notesnook.Notesnook -y

flatpak install flathub work.openpaper.Paperwork -y
# ocr management tool

flatpak install flathub com.github.jeromerobert.pdfarranger -y

flatpak install flathub xyz.rescribe.rescribe -y
# ocr
# an easy to use desktop tool for ocr of image files and pdfs etc

flatpak install flathub com.github.xournalpp.xournalpp -y
# take handwritten notes

# Settings

# System
flatpak install flathub org.virt_manager.virt-manager -y
# not verified

# Utilities
flatpak install flathub org.cryptomator.Cryptomator -y

#flatpak install flathub io.exodus.Exodus -y
# not verified
# not really used much - need more monies for it 

flatpak install flathub it.mijorus.gearlever -y
# bazzite has it by default
# aurora-dx does not
# an utility to manage AppImages

flatpak install flathub io.github.seadve.Kooha -y
# elegntly record your screen

flatpak install flathub net.sapples.LiveCaptions -y

flatpak install flathub org.localsend.localsend_app -y
# open source alternative to airdrop

flatpak install flathub com.nextcloud.desktopclient.nextcloud -y
# not verified

flatpak install flathub io.github.picocrypt.Picocrypt -y
# A very small, very simple, yet very secure encryption tool.

flatpak install flathub com.pot_app.pot -y
# ocr
# A text translation and OCR app

flatpak install flathub org.raspberrypi.rpi-imager -y
# not verified

#flatpak install flathub com.borgbase.Vorta -y
# need another server or nas for this

# Lost & found

#flatpak install flathub org.freac.freac -y
# uses old runtimes at the moment
# Audio converter and CD ripper
# tested and it works

# more new flatpaks to test


echo "Finished installing flatpaks for fresh Aurora-dx or Bazzite or atomic systems"