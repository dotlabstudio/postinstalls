# Ventoy Windows 11 To Go

On A Windows Machine

Use Diskpart to Create and mount 50GB VHD
```
diskpart
create vdisk file=c:\win11.vhd maximum=51200
select vdisk file=c:\win11.vhd
attach vdisk
```

Use Rufus:

Write Windows 11 ISO as Windows To Go image (GPT and UEFI) to the VHD.

and then unmount the vhd with diskpart
```
detach vdisk
```

VirtualBox:

Create a Windows 10 VM with the existing c:\win11.vhd.

Change VM Settings > System > Motherboard > Enable EFI.

Boot until language selection and shutdown VM.

Ventoy:

In Ventoy choose Option > Partition Style > GPT.

Under Device choose your USB-drive and click Install.

Explorer:

Manual format the created Ventoy partition as NTFS.

Place win11.vhd on your NTFS formatted Ventoy partition.

Place ventoy_vhdboot.img in ventoy folder on Ventoy partition.

source:
```
https://kbhost.nl/knowledgebase/ventoy-windows-11-to-go/
```