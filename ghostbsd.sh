#!/usr/bin/env bash

echo " this script setups a fresh ghostbsd system"

echo " update system "
sudo pkg update; sudo pkg upgrade -y

echo " install guest os tools "
sudo pkg install -y qemu-guest-agent

sudo sysrc qemu_guest_agent=“YES”

echo " setup sshd server"
sudo sysrc sshd_enable="YES"

echo " "
echo " install cli tools "
sudo pkg install -y git fortune-mod-FreeBSD-classic fastfetch wget curl htop vim yt-dlp lynis rkhunter chkrootkit clamav ouch tealdeer aspell hunspell en-aspell en-hunspell

echo " "
echo "update clamav"
sudo freshclam

echo " setup tailscale"
#installing tailscale
sudo pkg install -y tailscale

echo "start and enable tailscale services"
sudo service tailscaled enable
sudo service tailscaled start

#login tailscale
#sudo tailscale up

# echo "install development tools"
# sudo pkg install -g 'GhostBSD*-dev'

# source:
# https://ghostbsd-documentation-portal.readthedocs.io/en/latest/user/FAQ.html

# upgrading via cli commands
# https://ghostbsd-documentation-portal.readthedocs.io/en/latest/user/upgrading-guide.html

echo " setup gui apps"
sudo pkg install -y synapse filezilla croscorefonts nextcloudclient digikam krita simplescreenrecorder digikam obs-studio kdenlive libreoffice

echo " use control + space bar for synapse"
echo " finished setting up ghostbsd "

