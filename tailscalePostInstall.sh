#!/bin/bash
#
#tailscale post install script
# Log in, enable and start tailscaled 
sudo systemctl enable --now tailscaled 

# Start Tailscale! 
sudo tailscale up
