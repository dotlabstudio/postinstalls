#!/usr/bin/env bash

echo "This script will set a self hosted ente photo server"
cd /data/docker

# reference:
# https://help.ente.io/self-hosting/

git clone https://github.com/ente-io/ente
cd ente/server
docker compose up -d --build

echo "run the web client"
cd /data/docker
cd ente/web
git submodule update --init --recursive
yarn install
NEXT_PUBLIC_ENTE_ENDPOINT=http://localhost:8080 yarn dev

echo "if you open http://localhost:3000, you will be able to create an account on a Ente Photos web app running on your machine,"

echo " and this web app will be connecting to the server running on your local machine at localhost:8080."



