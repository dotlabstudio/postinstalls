# Apps to fix stupid windows updates

## Windows Update Fixer

Update Fixer is a lightweight, freeware app to automatically fix Windows Update.

```
https://winupdatefixer.com/
```

## InControl

InControl is a one-button utility which gives users control over
all non-security updates to their Windows 10 and 11 systems.

```
https://www.grc.com/incontrol.htm
```

## Windows Update Error Codes

```
https://learn.microsoft.com/en-us/windows/deployment/update/windows-update-error-reference
```