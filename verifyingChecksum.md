
# Verifying Checksum on All OS

## Linux

md5sum filename 

sha256sum filename

## MacOS

md5 filename 

shasum -a 256 filename

or use 

hashtab

## Windows

CertUtil -hashfile filename MD5 

CertUtil -hashfile filename SHA256

### Powershell

Get-FileHash -Algorithm MD5 filename

Get-FileHash -Algorithm SHA256 filename

or use

hashtab

