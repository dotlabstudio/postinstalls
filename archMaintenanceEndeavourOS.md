# EndeavourOS Maintenance / Update / Upgrade
  [Frequency: Every 1-2 months, or user discretion]

# UPDATE SYSTEM:
  [Frequency: Daily, Weekly, or user discretion]

source:
```
https://forum.endeavouros.com/t/do-i-need-to-constantly-maintain-endeavouros/65744/4

https://forum.endeavouros.com/t/a-complete-idiots-guide-to-endeavour-os-maintenance-update-upgrade/25184
```

```
yay
```

SORTING PACNEW/PACSAVE FILES, WHEN PROMPTED BY TERMINAL:
```
DIFFPROG=meld pacdiff
```

UPDATE ARCH MIRRORS:
```
sudo reflector --protocol https --verbose --latest 25 --sort rate --save /etc/pacman.d/mirrorlist
```

ATER UPDATING ARCH MIRRORLIST, UPDATE SYSTEM:
```
yay -Syyu
```

UPDATE EOS MIRRORS:
```
eos-rankmirrors --verbose
```

ATER UPDATING EOS MIRRORLIST, UPDATE SYSTEM:
```
yay -Syyu
```

CLEAN JOURNAL:
```
sudo journalctl --vacuum-time=4weeks
```

CLEAN CACHE (All Packages):
```
paccache -r
```

CLEAN CACHE (Uninstalled Packages):
```
paccache -ruk0
```

REMOVE ORPHANS:
```
yay -Yc
```

or
```
paru -c
```

or
```
pacman -Rns $(pacman -Qdtq)
```

if using the fish shell:
```
pacman -Rns (pacman -Qdtq)
```
