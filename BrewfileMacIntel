tap "borgbackup/tap"
tap "gromgit/fuse"
tap "homebrew/bundle"
tap "homebrew/cask-versions"
# ChatGPT in the terminal
brew "ata"
# Improved shell history for zsh, bash, fish and nushell
brew "atuin"
# Clone of cat(1) with syntax highlighting and Git integration
brew "bat"
# Rootkit detector
brew "chkrootkit"
# GNU File, Shell, and Text utilities
brew "coreutils"
# Modern, maintained replacement for ls
brew "eza"
# Like neofetch, but much faster because written mostly in C
brew "fastfetch"
# Simple, fast and user-friendly alternative to find
brew "fd"
# Play, record, convert, and stream audio and video
brew "ffmpeg"
# User-friendly command-line shell for UNIX-like operating systems
brew "fish"
# Infamous electronic fortune-cookie generator
brew "fortune"
# Distributed revision control system
brew "git"
# GNU Pretty Good Privacy (PGP) package
brew "gnupg"
# Manage your GnuPG keys with ease!
brew "gpg-tui"
# Command-line tool for generating regular expressions
brew "grex"
# Post-modern modal text editor
brew "helix"
# Improved top (interactive process viewer)
brew "htop"
# Tools and libraries to manipulate images in many formats
brew "imagemagick"
# Rainbows and unicorns in your console!
brew "lolcat"
# Clone of ls with colorful output, file type icons, and more
brew "lsd"
# Security and system auditing tool to harden systems
brew "lynis"
# Mac App Store command-line interface
brew "mas"
# Official Monero wallet and CPU miner
brew "monero"
# Ambitious Vim-fork focused on extensibility and agility
brew "neovim"
# Port scanning utility for large networks
brew "nmap"
# Painless compression and decompression for your terminal
brew "ouch"
# Swiss-army knife of markup format conversion
brew "pandoc"
# PDF processor written in Go
brew "pdfcpu"
# Modern replacement for ps written by Rust
brew "procs"
# Search tool like grep and The Silver Searcher
brew "ripgrep"
# Rootkit hunter
brew "rkhunter"
# Intuitive find & replace CLI
brew "sd"
# Prints a steam locomotive if you type sl instead of ls
brew "sl"
# Command-line interface for https://speedtest.net bandwidth tests
brew "speedtest-cli"
# Cross-shell prompt for astronauts
brew "starship"
# Very fast implementation of tldr in Rust
brew "tealdeer"
# Vi 'workalike' with many additional features
brew "vim"
# Internet file retriever
brew "wget"
# Show the current WiFi network password
brew "wifi-password"
# Monero (XMR) CPU miner
brew "xmrig"
# PDF viewer
brew "xpdf"
# Feature-rich command-line audio/video downloader
brew "yt-dlp"
# Deduplicating archiver with compression and authenticated encryption
brew "borgbackup/tap/borgbackup-fuse"
# Read-write NTFS driver for FUSE
brew "gromgit/fuse/ntfs-3g-mac"
# File system client based on SSH File Transfer Protocol
brew "gromgit/fuse/sshfs-mac"
# 3D creation suite
cask "blender"
# Web browser focusing on privacy
cask "brave-browser"
# Server and cloud storage browser
cask "cyberduck"
# Photography workflow application and raw developer
cask "darktable"
# Music player
cask "doppler"
# Desktop wallet for cryptocurrency assets
cask "exodus"
# Web browser
cask "firefox"
# Free and open-source image editor
cask "gimp"
# Free and open-source media player
cask "iina"
# Jellyfin desktop client
cask "jellyfin-media-player"
# Automation software
cask "keyboard-maestro"
# Free and open-source painting and sketching program
cask "krita"
# Grammar, spelling and style suggestions in all the writing apps
cask "languagetool"
# Official client for LBRY, a decentralised file-sharing and payment network
cask "lbry"
# File system integration
cask "macfuse"
# Untraceable cryptocurrency wallet
cask "monero-wallet"
# Desktop sync client for Nextcloud software products
cask "nextcloud"
# Privacy-focused note taking app
cask "notesnook"
# Open-source software for live streaming and screen recording
cask "obs"
# Instant messaging application focusing on security
cask "signal"
# Free, open-source, and completely encrypted notes app
cask "standard-notes"
# Web browser focusing on security
cask "tor-browser"
# Facilitates communication between the Trezor device and supported browsers
cask "trezor-bridge"
# Companion app for the Trezor hardware wallet
cask "trezor-suite"
# Multimedia player
cask "vlc"
# Create, manage, and run virtual machines
cask "vmware-fusion"
# Desktop Backup Client for Borg
cask "vorta"
# Rust-based terminal
cask "warp"
# Terminal emulator
cask "wave"
# Network protocol analyzer
cask "wireshark"
mas "Bitwarden", id: 1352778147
mas "ClipTools", id: 1619348240
mas "Collabora Office", id: 918120011
mas "CotEditor", id: 1024640650
mas "DaVinci Resolve", id: 571213070
mas "DuckDuckGo", id: 663592361
mas "Elmedia Video Player", id: 1044549675
mas "GarageBand", id: 682658836
mas "Honeygain", id: 1479049768
mas "iHash", id: 763902043
mas "iMovie", id: 408981434
mas "Infuse", id: 1136220934
mas "iStat Menus", id: 1319778037
mas "Keynote", id: 409183694
mas "LanguageTool", id: 1534275760
mas "LocalSend", id: 1661733229
mas "NetSpeedMonitor", id: 1575839740
mas "NextDNS", id: 1464122853
mas "Numbers", id: 409203825
mas "OpenShot video editor", id: 6443498794
mas "Pages", id: 409201541
mas "PhotoSync Companion", id: 418818452
mas "PicArrange", id: 1530678223
mas "Pine Player", id: 1112075769
mas "Tailscale", id: 1475387142
mas "The Battle for Wesnoth", id: 1450738104
mas "The Unarchiver", id: 425424353
mas "UTM", id: 1538878817
mas "Xcode", id: 497799835
