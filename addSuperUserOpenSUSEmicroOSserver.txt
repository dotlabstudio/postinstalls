## Add user to sudoers
echo "<username> ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/<username>

## Create ssh folder and populate authorized_keys for remote sshd
mkdir -pm 700 /home/<username>/.ssh

touch /home/<username>/.ssh/authorized_keys

chown <username>:<username> -R /home/<username>/.ssh
