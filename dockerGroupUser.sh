#!/usr/bin/env bash

echo "this script creates a docker group and add your user to the docker group"

# create the docker group and add your user:

echo "Create the docker group"
sudo groupadd docker

echo "Add your user to the docker group"
sudo usermod -aG docker $USER

echo "activate new group"
newgrp docker

echo "Log out and log back in so that your group membership is re-evaluated"
