This is my public repo for post install scripts and stuff for a fresh system - mainly linux systems like opensuse micro os or nixos etc.

For shell scripts ending with .sh - use chmod + x *.sh to make the scripts work.

Then Tuesday, 26 Mar 2024 at 18:24

```
./script.sh
```

 to run the script.

# For a Fresh system


# Change username default shell to fish

```
chsh -s /usr/bin/fish
```

# Change bash scripts to run on linux distributions

```
#!/usr/bin/env bash
```

# Quad9 DNS

```
https://www.quad9.net/
```

for the browsers:
```
https://dns.quad9.net/dns-query

```

To check that quad9 is setup:
```
https://on.quad9.net 
```

# Setup new ssh pair keys for a new machine and copy public key to a server

To generate a new ssh pair keys on on a new machine:

```
ssh-keygen -t ed25519 -a 100
```

To copy public ssh keys to a server
```
ssh-copy-id -i ~/.ssh/id_ed25519.pub $USER@<serverIPorName>
```

# Git Clone lynis software

```
git clone https://github.com/CISOfy/lynis
```

run lynis on new system
```
cd lynis && ./lynis audit system
```

from https://github.com/CISOfy/Lynis

# Setup ssh server after installing openssh-server

start ssh server
```
sudo systemctl start sshd
```

check status of ssh server
```
sudo systemctl status sshd
```

enable ssh server on startup or boot
```
sudo systemctl enable sshd
```

# Tealdeer Setup 

```
tldr  --update
```

# Atuin Setup

```
atuin import auto
```

# Checking hardware as superuser

BIOS or UEFI

```
[ -d /sys/firmware/efi] && echo EFI || echo Legacy
```

or

```
[ -d/sys/firmware/efi] && echo UEFI || echo BIOS && uname -mrs 
```


Is virtualization enabled?

```
grep --color -E "vmx|svm" /proc/cpuinfo
```

check disk

```
lsbk
```

```
cat /sys/block/sda/queue/rotational
```

output 1=HDD 0=sshd

GPU
```
lspci | grep -i --color `vga\|3d\|2d`
```

or

```
lspci | grep -i VGA 
```

CPU cores
```
lscpu
```

or

```
glxinfo -B 
```

verify CPU cores

```
nproc
```

Adjust makeopts as needed : MAKEOPTS="-j4"

RAM
```
free -hm
```

# Setup virt manager

Use the virtualVirtManager.sch script to start libvirt services. If on a debian like system use the virtual.sh script instead.

Before the script;

edit the /etc/libvirt/libvirtd.conf file with

```
sudo vim /etc/libvirt/libvirtd.conf
```


Set the domain socket group ownership to libvirt
```
unix_sock_group = "libvirt"
```

Set the UNIX socket permissions for the R/W socket
```
unix_sock_ro_perms = "0777"

unix_sock_rw_perms = "0770"
```

Just uncomment these sections in the libvirtd.conf file

source: https://www.mocaccino.org/docs/desktop/virt-manager/

# Install distrobox the git way

To install the latest version via git

```
curl -s https://raw.githubusercontent.com/89luca89/distrobox/main/install | sudo sh

# or using wget

wget -qO- https://raw.githubusercontent.com/89luca89/distrobox/main/install | sudo sh
```

For custom directory to install without sudo

```
curl -s https://raw.githubusercontent.com/89luca89/distrobox/main/install | sh -s -- --prefix ~/.local
```

or

```
wget -qO- https://raw.githubusercontent.com/89luca89/distrobox/main/install | sh -s -- --prefix ~/.local
```

from https://distrobox.it/

## Distrobox containers

```
distrobox create --name pkg --image registry.opensuse.org/opensuse/distrobox-packaging:latest 

distrobox create --name arch --image quay.io/toolbx/arch-toolbox:latest

distrobox create --name debian --image quay.io/toolbx-images/debian-toolbox:12

distrobox create --name kali --image docker.io/kalilinux/kali-rolling:latest

```

# Erase Apple Macbooks

## for Apple Silicon

search for erase for  Erase All Contents and Settings.

or

Choose Apple menu  > System Settings, then click General  in the sidebar. (You may need to scroll down.)

Click Transfer or Reset on the right, then click Erase All Contents and Settings.

In Erase Assistant, enter your administrator information and click Unlock.

Review items that will be removed in addition to your content and settings.

If your Mac has multiple user accounts, click the arrow next to your account name to review the items.

Click Continue, then follow the onscreen instructions.

## for intel or old Macbooks

Use Disk Utility to erase your Mac
Start up from macOS Recovery: turn on your Mac, then press and hold Command (⌘) and R immediately until you see an Apple logo or other image.

If asked, select a user you know the password for and enter their administrator password.

From the utilities window, select Disk Utility and click Continue.

Select Macintosh HD in the sidebar of Disk Utility. Can't see Macintosh HD?

Click the Erase button in the toolbar, then enter the requested details:

Name: Macintosh HD

Format: APFS or Mac OS Extended (Journaled), as recommended by Disk Utility

Click Erase Volume Group. If you can’t see this button, click Erase instead.

source: https://support.apple.com/en-gb/102639


# Macbooks

Get home brew 

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

from 

https://brew.sh/


## Updating in Homebrew

```
brew update; brew upgrade --greedy; brew cleanup
```

### Get list of applcations installed on MacOS with Homebrew

```
brew bundle dump --describe
```

### To restore MacOS software and homebrew apps from a Brewfile


cd into where the Brewfile is located first 

```
brew bundle -v ; brew cleanup ; brew doctor 
```

from https://twit.tv/shows/hands-on-mac/episodes/9

```
brew bundle install --file /path/to/Brewfile
```

# Install Linux on Apple Silicon Macbooks along with MacOS 

```
curl https://alx.sh | sh
```

from: 

https://asahilinux.org/fedora/


# Getting to a tty

logout of wayland or x11 session


Ctrl+Alt+F4

or

Ctrl+Alt+F2


## SCP 

Copy Files From Remote to Local:
```
scp <remote_username>@<IPorHost>:<PathToFile>   <LocalFileLocation>
```

# Fonts under Linux

Install or copy fonts to:
```
~/.local/share/fonts/
```

create the fonts folder:
```
mkdir -p ~/.local/share/fonts/
```

Finally, update the fontconfig cache (usually unnecessary as software using the fontconfig library does this):
```
fc-cache
```

source:
https://wiki.archlinux.org/title/Fonts#Manual_installation


# File Sharing under KVM

sudo mount -v -t virtiofs <tag_mount> <guest_homedir>

for example:

```
sudo mount -v -t virtiofs videos ~/Videos
```

# Brew app list for aurora-dx 


```
brew install yt-dlp fortune ouch mp3gain minisign speedtest-cli dos2unix aria2
```

# Changing user shells in fedora atomic desktops

```
sudo usermod --shell /path/to/shell <user>
```

# Add new or extra drives in fedora atomic desktops and other linux distributions

Create mount points first for drives

for example:
```
sudo mkdir /var/data
```

```
/var/opt
```
is in most os distributions


then change the Permissions for the drive
```
sudo chown $USER:$USER /var/data
```

Use a partition manager

create a gpt table

new brtfs volume with encryption and label

check everyone for mount 

apply the changes 


Open KDE Partition Manager

Locate the disk and partition you want to mount

Right click on the partition and click “Edit Mount Point”

Select “Identify by: UUID” (This will guarantee you mount THIS partition instead of a different one if the device nodes change for some reason)

Select a mounting path (You would want to use /var/mnt/data or something similar for permanent mounts)

Untick all the boxes in the graphical application if they are checked

Click “More…” and add extra options depending on what filesystem is on the partition (read the “Filesystem Arguments” section)

brfs options:
```
defaults,compress-force=zstd:3,noatime,lazytime,commit=120,space_cache=v2,nofail
```

ext4 options:
```
defaults,noatime,errors=remount-ro,nofail,rw,users,exec
```

Note: “Users can mount and unmount” is an optional setting.


Click OK on both windows to save the mount points.

A message will appear that the actions will edit /etc/fstab (Click “OK” to continue)

Mount the disk manually in KDE Partition Manager and enter your sudo password

Open the terminal to test the mounts by running the command:
```
sudo systemctl daemon-reload && sudo mount -a
```

If no errors appeared then it should be safe to reboot.



sources:
```
https://universal-blue.discourse.group/docs?topic=970

https://universal-blue.discourse.group/docs?topic=3780

```

# Auto mount encrypted partitions at boot

check the name of the encrypted partition:

```
lsblk
```

for example, lets assume it's sda1

next find the UUID of the encrypted drive:

```
sudo cryptsetup luksUUID /dev/sda1
```

let's assume it returned:

fd3c01ad-0e59-4bc1-9bda-7c61e00b36cf

Time to edit the /etc/crypttab file:
```
sudo vim /etc/crypttab
```

add the following line and save it:
```
sda1 /dev/disk/by-uuid/<UUID of block device> none nofail luks
```

it should look something like this

sda1 /dev/disk/by-uuid/fd3c01ad-0e59-4bc1-9bda-7c61e00b36cf none nofail luks


format of the entry:

- sda1 is the name of the encrypted drive
- fd3c01ad-0e59-4bc1-9bda-7c61e00b36cf is the UUID we got from sudo cryptsetup luksUUID /dev/sda1
- none - means we do not use a key file but a password or pass phrase
- nofail - must have in case the drives can not mounted and the system can continue to boot
- luks - is the disk encryption type


source:
```
https://averagelinuxuser.com/auto-mount-encrypted-partitions-at-boot/
```

# Tailscale on LXC Containers

source:
```
https://tailscale.com/kb/1130/lxc-unprivileged
```

To bring up Tailscale in an unprivileged container, access to the /dev/tun device can be enabled in the config for the LXC. For example, using Proxmox 7.0 to host as unprivileged LXC with ID 112, the following lines would be added to /etc/pve/lxc/112.conf:

```
lxc.cgroup2.devices.allow: c 10:200 rwm
lxc.mount.entry: /dev/net/tun dev/net/tun none bind,create=file
```

If the LXC is already running it will need to be shut down and started again for this change to take effect.

Once /dev/tun is available, the Tailscale Linux package can be installed in the system running within the LXC.

# Add new user in Linux

Make New User First:
```
adduser <username>
```

Add User to the Sudo Group:
```
usermod -aG sudo <username>
```

# Add User to the Docker group

```
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
```

# Proxmox VE Helper-Scripts

website:
```
https://tteck.github.io/Proxmox/
```

github:
```
https://github.com/tteck/Proxmox
```

Scripts must be run in the PVE shell

Proxmox VE Post Install:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/misc/post-pve-install.sh)"
```

yes to the defaults is good.

Docker LXC:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/ct/docker.sh)"
```

Debian LXC:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/ct/debian.sh)"
```

iVentoy LXC:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/ct/iventoy.sh)"
```

container must be Privileged.


Tailscale for existing LXC containers:
```
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/misc/add-tailscale-lxc.sh)"
```

# Docker Compose

file:
```
docker-compose.yaml
```

run it:
```
docker compose up -d
```

check the logs:
```
docker compose logs
```

stop it:
```
docker compose down
```

to use a specific docker compose file:
```
docker compose -f docker-compose-traefik.yaml up -d
```

for example 

sources:
```
https://techhut.tv/7-docker-basics-for-beginners/
```

