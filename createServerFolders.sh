#!/usr/bin/env bash

echo " This script just create server directories for a fresh system"

sudo mkdir -p /var/data
sudo chown $USER:$USER /var/data

mkdir -p /var/data/docker

mkdir -p /var/data/media/movies

mkdir -p /var/data/media/music

mkdir -p /var/data/media/photos

mkdir -p /var/data/media/shows

# cosmos
mkdir -p /var/data/docker/cosmos

# jellyfin
mkdir -p /var/data/docker/jellyfin/cache
mkdir -p /var/data/docker/jellyfin/config

# navidrome
mkdir -p /var/data/docker/navidrome/navidrome-data 
mkdir -p /var/data/docker/navidrome/navidrome-data/cache

# nextcloud
mkdir -p /var/data/docker/nextcloud/nextcloud-config
mkdir -p /var/data/docker/nextcloud/nextcloud-data
mkdir -p /var/data/docker/nextcloud/nextcloud-apps

# photoprism
mkdir -p /var/data/docker/photoprism/photoprism-data

# plex
mkdir -p /var/data/docker/plex/plex-data

# macos docker
mkdir -p /var/data/docker/osx/storage


