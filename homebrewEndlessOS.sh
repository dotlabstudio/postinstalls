#!/usr/bin/env bash

echo " this script will install homebrew on endless os or another atomic based system "

# reference:
# https://community.endlessos.com/t/homebrew-on-endless/19126

# script works but brew install does not work as 2025-02-03

echo " first install curl to local bin "
sudo mkdir -p /usr/local/bin
sudo wget -O /usr/local/bin/curl https://github.com/moparisthebest/static-curl/releases/download/v8.11.0/curl-amd64
sudo chmod +x /usr/local/bin/curl

echo " "
echo " Install brew "
echo " it’s installed with the same command as given on their homepage,"
echo " https://brew.sh/ "
echo " but you have to make sure to press Ctrl-D when asked, as we want a per-user local installation "
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

echo " "
echo " environment variables into your shells startup script "
echo PATH=${PATH}:~/.linuxbrew/bin >> ~/.bashrc
export HOMEBREW_CURL_PATH=/usr/local/bin/curl >> ~/.bashrc
export MANPATH=${MANPATH}:~/.linuxbrew/share/man >> ~/.bashrc

echo " "
echo " homebrew is setup for endless os or atomic based system"