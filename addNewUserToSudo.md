# Add new user in Linux

Make New User First:
```
adduser <username>
```

Add User to the Sudo Group:
```
usermod -aG sudo <username>
```