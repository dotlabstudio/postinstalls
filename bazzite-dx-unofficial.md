# Unofficial bazzite-dx 

```
https://github.com/Sparkrai/bazzite-dx
```

To rebase an existing atomic Fedora installation to the latest build:

First choose the flavor of the image you'd like to install (either bazzite-dx or bazzite-gnome-dx)

Then rebase to the unsigned image, to get the proper signing keys and policies installed:
```
rpm-ostree rebase ostree-unverified-registry:ghcr.io/sparkrai/bazzite-dx:latest
```

Reboot to complete the rebase:
```
systemctl reboot
```

Then rebase to the signed image, like so:
```
rpm-ostree rebase ostree-image-signed:docker://ghcr.io/sparkrai/bazzite-dx:latest
```

Reboot again to complete the installation
```
systemctl reboot
```
