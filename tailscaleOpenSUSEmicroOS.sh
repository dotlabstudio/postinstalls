#!/bin/bash
#installing tailscale on
#openSUSE MicroOS

# Add the tailscale repository 

sudo zypper ar -g -r https://pkgs.tailscale.com/stable/opensuse/tumbleweed/tailscale.repo 

# Install Tailscale 
sudo transactional-update pkg install tailscale 

# Reboot your PC
sudo systemctl reboot

#moved this section to a post install script for systemd systems
# Log in, enable and start tailscaled 
#sudo systemctl enable --now tailscaled 
#
# Start Tailscale! 
#sudo tailscale up
