#!/usr/bin/env bash

echo "This script will setup my stuff for a fresh install of solus"

echo "update system first"
sudo eopkg upgrade -y

echo "cli programs"

# wget and yt-dlp already installed

sudo eopkg install git neofetch fish gufw vim htop powerline powerline-fonts tealdeer font-firacode-nerd -y

echo "install development tools"

sudo eopkg install -c system.devel -y

echo "install linux headers"

sudo eopkg install linux-current-headers -y

echo "Develop"
sudo eopkg install meld -y

echo "Education"
sudo eopkg install marble qgis -y

echo "Games"
sudo eopkg install wesnoth gnome-chess gnuchess supertux supertuxkart xonotic -y

echo "Graphics"
sudo eopkg install digikam darktable gimagereader gimp inkscape kcolorchooser kolourpaint krita -y

#blender crashes on slous

echo "Internet"
sudo eopkg install brave filezilla lagrange onionshare qbittorrent remmina signal-desktop torbrowser-launcher -y

echo "Multimedia"
sudo eopkg install handbrake k3b kdenlive picard qpwgraph strawberry vlc -y

echo "Office"
sudo eopkg install crow-translate pdfarranger xournalpp -y

echo "Utilities"
sudo eopkg install cryptomator helix nextcloud-client vorta  -y

echo "setup ssh server"
sudo eopkg install openssh-server -y

sudo systemctl start sshd
sudo systemctl enable sshd

echo "copy harden ssh config to the system"
sudo cp issue.net /etc/issue.net 
#
sudo cp hardenSSH.conf /etc/ssh/sshd_config
# /etc/ssh/sshd_config.d/
# harden configs does not work with solus for some reason

echo "setup obs studio with video loopback"
sudo eopkg install obs-studio v4l2loopback-current libuiohook -y

echo "setup virt manager"
sudo eopkg install virt-manager -y

sudo systemctl start libvirtd

sudo systemctl enable libvirtd

echo "setup virtualbox"
sudo eopkg install virtualbox-current virtualbox-guest-additions-iso -y

sudo gpasswd -a $USER vboxusers

# only need for guest systems
#sudo usermod -aG vboxsf $USER
#sudo gpasswd -a $USER vboxsf

echo "install ms core fonts"
sudo eopkg bi --ignore-safety https://raw.githubusercontent.com/getsolus/3rd-party/master/desktop/font/mscorefonts/pspec.xml -y

sudo eopkg install mscorefonts*.eopkg -y

echo "setup wireshark for solus"
sudo eopkg install wireshark -y

sudo gpasswd -a $USER wireshark
sudo setcap cap_dac_override,cap_net_admin,cap_net_raw+eip /usr/bin/dumpcap

echo "Finishing setting up my new solus system"

