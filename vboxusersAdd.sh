#!/usr/bin/env bash

# add user to vboxusers group
sudo usermod -a -G vboxusers $USER
