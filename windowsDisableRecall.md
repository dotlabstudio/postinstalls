# Disable Windows Recall

On the command line, check for it:
```
dism /online /Get-Featureinfo /FeatureName:Recall
```

disable recall:
```
dism /online /Disable-Feature /FeatureName:Recall
```

Under Group policy:

Check for Administrative templates 

Windows ai

Enable turn off saving snapshots for windows

reference:
```
https://briteccomputers.co.uk/posts/microsoft-recall-mandatory-on-windows-11-24h2/
```

