# Setup for a new micro-os server with docker and cockpit

## Setup a new super user first

Make New User First and Create User Home Directory:
```
adduser -m <username>
```

Set password 
```
password <username>
```

## Add user to sudoers
```
echo "<username> ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/<username>
```

## Install git and fish first
```
sudo transactional-update --non-interactive pkg install fish
```

## Add tailscale

enter shell:
```
sudo transactional-update shell 
```

### Add the Tailscale repository:
```
sudo zypper ar -g -r https://pkgs.tailscale.com/stable/opensuse/tumbleweed/tailscale.repo
```
### refresh the repos
```
sudo zypper ref
```
### exit the transactional-update shell
```
exit 
```

## Add packages to my base install

### install my base image - this should only needed to once only - rest of the time - use flatpaks or distrobox
```
sudo transactional-update --non-interactive pkg install fastfetch htop yt-dlp eza bat lsd ouch gstreamer-1.20-plugin-openh264 tailscale ripgrep fira-code-fonts fontawesome-fonts procs sd tealdeer jetbrains-mono-fonts hunspell hunspell-tools aspell aspell-en lynis sysstat rkhunter clamav fortune unrar dirmngr spice-vdagent spice-webdavd docker docker-buildx docker-compose docker-rootless-extras
```

# curl, wget, vim and qemu-guest-agent are already installed on opensuse kalpa

### Post isntall script for tailscale

use the script

### Add user to docker group

use the script


## Add cockpit to server
```
sudo transactional-update pkg install patterns-microos-cockpit cockpit-pcp cockpit-selinux
```

### Enable cockpit service
```
sudo systemctl enable --now cockpit.socket
```

## Update the System
```
sudo zypper clean && sudo zypper ref && sudo transactional-update dup
```



