## Patching Windows 7 in 2024

1. Install Windows 7 Pro

2. Install all drivers required

3. Updates these Windows 7 updates manually from 

catalog.update.microsoft.com

https://catalog.update.microsoft.com/Home.aspx

KB3138612

KB4490628

kb4474419

KB3125574

KB5011649


3. Install all updates from windows update inside windows

4. Install updates manually from catalog

catalog.update.microsoft.com

KB5007236

KB5008244

KB5018454

KB5020000

KB5021291

KB5022338

KB5017361

KB5034831

KB5034620

KB5034167


sources:

https://briteccomputers.co.uk/posts/can-you-still-use-windows-7-in-2024-2/

and 

Tech Guy One youtube channel @GenercTechSupport

### Global wallpapers in windows 7

```
C:\\Windows\Globalization\MCT
```


## Activating Windows via the CMD

```
slmgr /ipk <productKey>

slmgr /skms kms8.msguides.com

slmgr /ato
```

source:

https://msguides.com/windows-10

Use the command “slmgr /ipk yourlicensekey” to install a license key 

Set KMS machine address.
Use the command “slmgr /skms kms8.msguides.com” to connect to a KMS server.

The last step is to activate your Windows using the command “slmgr /ato”.

If you see the error 0xC004F074, it means that your internet connection is unstable or the server is busy. 

Please make sure your device is online and try the command “ato” again until you succeed.

To display very basic license and activation information about the current system, run the following command. This command tells you the edition of Windows, part of the product key so you can identify it, and whether the system is activated.

```
slmgr.vbs /dli
```

To display more detailed license information--including the activation ID, installation ID, and other details--run the following command:

```
slmgr.vbs /dlv
```

View the License Expiration Date
To display the expiration date of the current license, run the following command. This is only useful for Windows system activated from an organization's KMS server, as retail licenses and multiple activation keys result in a perpetual license that won't expire. If you haven't provided a product key at all, it'll give you an error message.

```
slmgr.vbs /xpr
```

sources:
```
https://www.lukastechs.net/2020/09/permanently-activate-windows-10-using-cmd.html

https://www.howtogeek.com/245445/how-to-use-slmgr-to-change-remove-or-extend-your-windows-license/
```
