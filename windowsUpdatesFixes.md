# Fix Corrupted Windows Updates

Sometimes Windows Update files get corrupted and the user is not able to download the files again or install the corrupted update files. In that case, we need to run a DISM command to fix the corrupted Windows Update. Here are the steps:

Launch the Command Prompt.
Run the following cmd:
```
dism /Online /Cleanup-image /Restorehealth
```

After successfully running this command, try force downloading the updates again, and the Windows Update should start working again.
Hopefully, this will be useful in situations where you want to automate certain Windows functions.
You can use these commands to fix things:
```
DISM /ONLINE /CLEANUP-IMAGE /SCANHEALTH 

DISM /ONLINE /CLEANUP-IMAGE /CHECKHEALTH

DISM /ONLINE /CLEANUP-IMAGE /RESTOREHEALTH

Sfc /Scannow 
```