#!/usr/bin/env bash 

# This script installs cuda stuff for debian
# use debian testing for vibe 
# source: 
# https://developer.nvidia.com/cuda-downloads

echo " install cuda toolkit from native repos"
sudo apt install -y nvidia-cuda-toolkit

echo " Download Base Installer for Linux Debian 12 x86_64 "

wget https://developer.download.nvidia.com/compute/cuda/repos/debian12/x86_64/cuda-keyring_1.1-1_all.deb

sudo dpkg -i cuda-keyring_1.1-1_all.deb
sudo apt update
sudo apt -y install cuda-toolkit

echo " NVIDIA Driver  "
sudo apt install -y nvidia-open