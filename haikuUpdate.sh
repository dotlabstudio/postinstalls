#!/usr/bin/env bash

echo "updating haiku"
echo " "

pkgman full-sync

# Update to the latest packages
# or use
# SoftwareUpdater		A tool to update software packages and Haiku itself.