# Windows Server as a Desktop

## Disable Server Manager From Startup

Server Manager Properties: 

Open Server Manager,

click Manage, and then Server Manager Properties. 

Uncheck the box next to Do not start Server Manager automatically at logon and click OK.


Command Line: Use the following command to disable Server Manager from starting at login:
```
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\ServerManager" /v StartAtLogon /t REG_DWORD /d 0 /f
```

## Disable The Shutdown Event For Windows Servers or Suppressing the "reason" for shutdown on Windows Server

Open "Group Policy Management Console" 

can be opened via Win+R and then executing 
```
gpedit.msc
```

navigate to Computer Configuration >> Administrative Templates >> System 

search for 
```
Display Shutdown Event Tracker
```

in All Settings

Then Disable It.

The above works well.

or command line (not tested):
```
reg.exe add "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Reliability" /v ShutDownReasonOn /t REG_DWORD /d 0 /f
```

