# This is my portable bookmarks for all systems including fresh ones

# DistroWatch
```
https://distrowatch.com/
```

# Ventoy
```
https://www.ventoy.net/en/index.html
```

# Flathub is the App Store for Linux
```
https://flathub.org/
```

# Docker Hub is the world's easiest way to create, manage, and deliver your team's container applications.
```
https://hub.docker.com/
```

# NAS Compares Tools
```
https://nascompares.com/
```

# UniGetUI (formerly WingetUI)

The Graphical Interface for your package managers

```
https://www.marticliment.com/unigetui/
```

UniGetUi on Github 
```
https://github.com/marticliment/UniGetUI/
```

# Installing Chocolatey
```
https://chocolatey.org/install
```

## install chocolatey via cmd line
```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

# Vibe - Transcribe. on Your Own.
```
https://thewh1teagle.github.io/vibe/
```

# Vibe Github repo
```
https://github.com/thewh1teagle/vibe/
```

# Vibe Models
```
https://github.com/thewh1teagle/vibe/blob/main/docs/MODELS.md#vibe-models
```

# Tailscale
```
https://tailscale.com/
```

## Tailscale on Proxmox Host
```
https://tailscale.com/kb/1133/proxmox
```



## Search Engines

### Brave Search
```
https://search.brave.com/
```

### Garuda SearX
```
https://searx.garudalinux.org/
```

### Duck Duck Go
```
https://duckduckgo.com/
```

### Qwant
```
https://www.qwant.com/
```

### Startpage
```
https://www.startpage.com/
```

### Chat GPT mini
```
https://chatgpt.com/
```

# Linux Forums

## Universal Blue Forums

```
https://universal-blue.discourse.group/
```

## EndeavourOS Forum 
```
https://forum.endeavouros.com/
```

## OpenSUSE English Forums
```
https://forums.opensuse.org/c/english/6
```

### Fedora Magazine 
```
https://fedoramagazine.org/
```

### NixOS Forum
```
https://discourse.nixos.org/
```

#### Search for NixOS packages
```
https://search.nixos.org/packages
```

# Operating systems

## Aurora-dx 
```
https://getaurora.dev/
```

## Bazzite
```
https://bazzite.gg/
```

### KDE Partition Manager Auto-Mount Guide
```
https://docs.bazzite.gg/Advanced/KDE_Partition_Manager_Auto_Mount_Guide/?h=aut
```

#### BTRFS additional options
```
defaults,compress-force=zstd:3,noatime,lazytime,commit=120,space_cache=v2,nofail
```

#### Ext4 additional options
```
defaults,noatime,errors=remount-ro,nofail,rw,users,exec
```

#### Test the new auto mount second drive
```
sudo systemctl daemon-reload && sudo mount -a
```


## Debian
```
https://www.debian.org/
```

## OpenSUSE
```
https://www.opensuse.org/
```

## MocaccinoOS
```
https://www.mocaccino.org/
```

### MocaccinoOS Docs 
```
https://www.mocaccino.org/docs/desktop/usage/
```

## 21 things to do after installing Solus
```
https://averagelinuxuser.com/after-installing-solus/
```


# Server Operating Systems 

## Proxmox 
```
https://www.proxmox.com/en/downloads
```

### virtio iso
```
https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/
```

### Windows Server ISO 
```
https://massgrave.dev/windows_server_links
```

### Windows LTSC ISO
```
https://massgrave.dev/windows_ltsc_links
```

### Verify ISO
```
https://files.rg-adguard.net/search
```

### Proxmox VE Helper-Scripts Github
```
https://github.com/tteck/Proxmox
```

### Proxmox VE Helper-Scripts Website 
```
https://tteck.github.io/Proxmox/
```

### OSX-PROXMOX - Run macOS on ANY Computer - AMD & Intel
```
https://github.com/luchina-gabriel/OSX-PROXMOX
```

#### iCloud for Proxmox macOS
```
https://www.icloud.com/
```

### Memory’s Tech Tips’ Unattended Windows Installation
```
https://github.com/memstechtips/UnattendedWinstall
```


## Cosmos Server 
```
https://github.com/azukaar/cosmos-Server/
```

### Cosmos Docs
```
https://cosmos-cloud.io/
```

### Cosmos Website 
```
https://cosmos-cloud.io/
```

## Start-OS
```
https://github.com/Start9Labs/start-os
```

### Ubuntu Hardening 
```
https://github.com/konstruktoid/hardening
```

### Lynis 
```
https://github.com/CISofy/lynis
```


# Self Host Services

## Jellyfin
```
https://jellyfin.org/docs/general/server/media/movies
```

## Nexcloud
```
https://nextcloud.com/install/#instructions-server
```

## Navidrome
```
https://www.navidrome.org/docs/usage/configuration-options/
```

## Ente Photos Server
```
https://help.ente.io/self-hosting/guides/
```

## Monero 
```
https://www.getmonero.org/get-started/mining/
```

### Run and mine on a p2pool Node
```
https://sethforprivacy.com/guides/run-a-p2pool-node/
```

## Awesome Open Source Projects YouTube Channel Website
```
https://wiki.opensourceisawesome.com/
```

## OSX (macOS) inside a Docker container.
```
https://github.com/dockur/macos
```


# AI 

## Venice ai - Private and Uncensored AI - don't need an account
```
https://venice.ai/
```

## Runaway AI 
```
https://runwayml.com/
```

# Hardware

## Framework laptop
```
https://frame.work/au/en
```

## Unify Drive UT2 Mobile NAS 
```
https://nascompares.com/2024/09/25/unifydrive-ut2-mobile-nas-drive-review/
```

## Unify Drive UT2 Website 
```
https://ut2.unifydrive.co/
```

### Unify Drive website 
```
https://unifydrive.co/
```

### QubeOS Hardware
```
https://www.qubes-os.org/doc/certified-hardware/
```
### HP ZBook Fury
### up to G11 as 2024-10-23
### has OLED Touchscreen and Three Button Touchpad
```
https://www.hp.com/us-en/workstations/zbook-fury.html
```

### HP ZBook Fury 16 G11 Mobile Workstation PC
```
https://www.hp.com/au-en/workstations/zbook-fury.html
```

## Fedora Asahi Remix
```
https://asahilinux.org/fedora/
```

### To install fedora asahi Remix
```
curl https://alx.sh | sh
```

Support up to M2 as 2024-10-25

# Windows To Go

# Ventoy Windows 11 To Go
```
https://kbhost.nl/knowledgebase/ventoy-windows-11-to-go/
```

# Education

## University of the People
```
https://www.uopeople.edu/
```

# Converters

## Roman Numerals Converter
```
https://www.convertit.com/Go/Maps/Calculators/Math/Roman_Numerals_Converter.ASP
```

# Wallpapers

## flicker
```
https://www.flickr.com/photos/tags/flicker/
```

```
https://www.flickr.com/
```

## Artvee
```
https://artvee.com/
```

## National Gallery of Art
```
https://www.nga.gov/
```

## TinEye Reverse Image Search
```
https://www.tineye.com/
```

# Futo wiki
```
https://wiki.futo.org/wiki/Introduction_to_a_Self_Managed_Life:_a_13_hour_%26_28_minute_presentation_by_FUTO_software
```

# Consumer Action Taskforce
```
https://wiki.rossmanngroup.com/wiki/Main_Page
```



