#!/usr/bin/env bash

echo "this script prep ubuntu or debian testing for vibe for command line use"
echo " "

echo "update the system first"
sudo apt update && sudo apt upgrade -y

echo "install stuff from the vibe github page"
sudo apt-get install xvfb -y

echo "fix appmenu module load error"
sudo apt install appmenu-gtk-module-common

echo " Gtk-Message: 20:35:42.484: Failed to load module colorreload-gtk-module " 
sudo apt install kde-config-gtk-style -y

echo " setup fake display "
Xvfb :1 -screen 0 1024x768x24 & 
export DISPLAY=1


