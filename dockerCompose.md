file:
```
docker-compose.yaml
```

run it:
```
docker compose up -d
```

check the logs:
```
docker compose logs
```

stop it:
```
docker compose down
```

to use a specific docker compose file:
```
docker compose -f docker-compose-traefik.yaml up -d
```

for example 

sources:
```
https://techhut.tv/7-docker-basics-for-beginners/
```
