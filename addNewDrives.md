Create mount points first for drives
for example:
```
sudo mkdir /var/data
```

/var/opt is across most os distributions 

then change the Permissions for the drive
```
sudo chown $USER:$USER /var/data
```

Use a partition manager

create a gpt table

new brtfs volume with encryption and label

check everyone for mount

apply the changes

Open KDE Partition Manager

Locate the disk and partition you want to mount

Right click on the partition and click “Edit Mount Point”

Select “Identify by: UUID” (This will guarantee you mount THIS partition instead of a different one if the device nodes change for some reason)

Select a mounting path (You would want to use /var/mnt/data or something similar for permanent mounts)

Untick all the boxes in the graphical application if they are checked

Click “More…” and add extra options depending on what filesystem is on the partition (read the “Filesystem Arguments” section)
brfs options:
```
defaults,compress-force=zstd:3,noatime,lazytime,commit=120,space_cache=v2,nofail
```

ext4 options:
```
defaults,noatime,errors=remount-ro,nofail,rw,users,exec
```

Note: “Users can mount and unmount” is an optional setting.

Click OK on both windows to save the mount points.

A message will appear that the actions will edit /etc/fstab (Click “OK” to continue)

Mount the disk manually in KDE Partition Manager and enter your sudo password

Open the terminal to test the mounts by running the command:
```
sudo systemctl daemon-reload && sudo mount -a
```

If no errors appeared then it should be safe to reboot.
sources:
```
https://universal-blue.discourse.group/docs?topic=970

https://universal-blue.discourse.group/docs?topic=3780
```