# Removing Pinned EOL System Flatpak Runtimes in ublue


Check if you can remove unused runtimes. This did not fix the problem in my case, but it is worth trying.
```
flatpak uninstall --unused
```

Check if some flatpak application is dependent on flatpak runtime:
```
flatpak list --app --columns=application,runtime | grep -E "org.gnome.Platform/x86_64/45|org.kde.Platform/x86_64/6.6"
```
in my case nothing was displayed, so no flatpak application is dependent on those specific runtimes.

Uninstall runtimes.
```
flatpak uninstall -y org.gnome.Platform/x86_64/45
flatpak uninstall -y org.kde.Platform/x86_64/6.6
```

Now check if all of your flatpak applications runs successfully.
```
flatpak list --app | gawk '{print "flatpak run " $2}
```
Execute every line you get from above command and check if flatpak application starts up. If all of them starts up, then uninstalled runtime did not have any effect. If for some reason one of flatpak do not starts up, then uninstall that particular flatpak and install it back. When flatpak application is installed it automatically installs runtimes.

Retry ujust update and there should be no problem

source:
```
https://universal-blue.discourse.group/t/system-flatpak-runtimes-eol/5132/7
```

