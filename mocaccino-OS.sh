#!/usr/bin/env bash

echo "update repo and update system for Mocaccino OS"
sudo luet repo update && sudo luet upgrade -y

echo "add the community repo"
echo " "

sudo luet install -y repository/mocaccino-community-stable

echo "refresh the repos"
sudo luet repo update

# mocaccino crashes with tailscale and the latest kernel
#echo "install tailscale"
#sudo luet install utils/tailscale -y

#echo "Use systemctl to enable and start the service"

#sudo systemctl enable --now tailscaled

echo " "
echo "install fish shell"
sudo luet install apps/fish -y

echo "change the shell to fish"
chsh -s /bin/fish

echo " "
echo " install spellcheckers "
sudo luet install apps/myspell-dicts -y

echo " setup flatpak "
sudo luet install apps/flatpak -y

echo " setup flathub repo "
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

echo " setup docker "
sudo luet install container/docker systemd-service/dockerd -y

echo " enable docker "
sudo systemctl start docker
sudo systemctl enable docker

echo " setup docker group "
sudo usermod -aG docker $USER

echo "list the kernels"
echo "please wait"
sudo mos kernel-switcher list

echo "switch to the latest mainline kernel"

sudo mos kernel-switcher switch kernel/mocaccino-full -y


# see https://github.com/mocaccinoOS/community-repository
