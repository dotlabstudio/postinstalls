# Tailscale on LXC Containers

source:
```
https://tailscale.com/kb/1130/lxc-unprivileged
```

To bring up Tailscale in an unprivileged container, access to the /dev/tun device can be enabled in the config for the LXC. For example, using Proxmox 7.0 to host as unprivileged LXC with ID 112, the following lines would be added to /etc/pve/lxc/112.conf:

```
lxc.cgroup2.devices.allow: c 10:200 rwm
lxc.mount.entry: /dev/net/tun dev/net/tun none bind,create=file
```

If the LXC is already running it will need to be shut down and started again for this change to take effect.

Once /dev/tun is available, the Tailscale Linux package can be installed in the system running within the LXC.