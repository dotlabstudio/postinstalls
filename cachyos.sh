#!/usr/bin/env bash

echo "post install script for fresh cachyos on REAL HARDWARE"
echo "cahyos already has fish and paru installed"

echo " "
echo "update cachyos system first"
paru -Syyu --noconfirm

echo " "
echo " setup snapper on cachyos "
paru -Syyu --noconfirm cachyos-snapper-support

echo " "
echo "Enable global menu:"
paru -Syyu --noconfirm --needed appmenu-gtk-module libdbusmenu-glib 

echo " "
echo "Enable trim operations on SSD/NVME:"
sudo systemctl enable --now fstrim.timer

echo " "
echo " update databases"
sudo updatedb
tldr --update 

echo " "
echo "install my cli tools"
paru -Syyu --noconfirm --needed lynis sysstat rkhunter arch-audit
paru -Syyu --noconfirm --needed wayland-protocols plasma-wayland-protocols

# not sure if this still needed but it's commented out beacuse it's from the aur not the standard repos
#paru -Syyu --noconfirm --needed xwaylandvideobridge

paru -Syyu --noconfirm --needed cowfortune clamav htop ouch yt-dlp 

echo " "
echo "install spellcheckers"
paru -Syyu --noconfirm --needed aspell aspell-en hunspell-en_au hunspell-en_gb

echo " "
echo "update clamav"
sudo freshclam

echo " "
echo "install my apps from standard repos "
paru -Syyu --noconfirm --needed minder marble wesnoth gnome-chess gnuchess knights blender converseen digikam darktable 

paru -Syyu --noconfirm --needed gimagereader-qt gimp inkscape kcolorchooser kolourpaint krita brave-bin filezilla

paru -Syyu --noconfirm --needed monero monero-gui p2pool xmrig qbittorrent torbrowser-launcher handbrake haruna signal-desktop

paru -Syyu --noconfirm --needed k3b dvd+rw-tools cdrtools kamoso kdenlive obs-studio qpwgraph strawberry

paru -Syyu --noconfirm --needed libreoffice-fresh pdfarranger xournalpp kcalc kooha localsend 

echo " separate out nextcloud client as it automatically starts on login "
paru -Syyu --noconfirm --needed nextcloud-client

echo " "
echo "setup tailscale"
paru -Syyu --noconfirm --needed tailscale

sudo systemctl enable --now tailscaled

# need KDE Partition manager for adding secondary drives
paru -Syyu --noconfirm --needed partitionmanager

#enable ssh server or disable sshd server
echo " "
echo "setup ssh server"
echo " with ssh server enabled - lynis score is 71"
sudo systemctl start sshd && sudo systemctl enable sshd

#echo "disable ssh server"
echo " "
echo " with ssh server disable - lynis score is 66"
# sudo systemctl disable sshd

echo " "
echo " copy harden ssh setup"
sudo cp issue.net /etc/issue.net
sudo cp hardenSSH.conf /etc/ssh/sshd_config.d/

#echo " "
#echo " use the firewall.sh scrpt for ufw rules if enabling a ssh server "
#echo " ufw is already enabled and active on cachyos by default "
#paru -Syyu --noconfirm --needed gufw

echo " "
echo " install microsoft font from cachyos repos"
paru -Syyu --noconfirm --needed ttf-ms-fonts


echo " "
echo "install my list of apps from aur list"
paru -Syyu --noconfirm --needed vibe-bin
paru -Syyu --noconfirm --needed synology-drive
echo " "
#echo "notesnook will take awhile to compile - be patient"
paru -Syyu --noconfirm --needed notesnook-bin
# notesnook might be better as a flatpak


# below - not really used much
#paru -Syyu --noconfirm --needed livecaptions
# hit or miss in compiling this from the aur

#paru -Syyu --noconfirm --needed glaxnimate
#paru -Syyu --noconfirm --needed switcheroo-gtk4 
#paru -Syyu --noconfirm --needed lagrange-bin
#paru -Syyu --noconfirm --needed standardnotes-bin
#paru -Syyu --noconfirm --needed cryptomator-bin 
#paru -Syyu --noconfirm --needed exodus

echo " "
#echo " setup cosmic desktop as another de"
#paru -Syyu --noconfirm --needed cosmic
# comsic needs a autostart settings section

echo " "
echo "finished setting up a fresh cachyos system"
