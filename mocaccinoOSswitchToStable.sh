#!/usr/bin/env bash

echo "remove the development repos first"

sudo luet uninstall -y repository/mocaccino-extra repository/mocaccino-kernel repository/mocaccino-desktop repository/mocaccino-os-commons

echo " "
echo "add the stable repos"

sudo luet install -y --nodeps repository/mocaccino-extra-stable repository/mocaccino-kernel-stable repository/mocaccino-desktop-stable repository/mocaccino-os-commons-stable

