#!/usr/bin/env bash

echo "script to setup virt manager on an arch based distro with paru like cachyos"

echo " "
echo "get the kernel headers"
paru -Syyu --noconfirm --needed linux-headers net-tools

echo " "
echo "setup virt manager"
paru -Syyu --noconfirm --needed virt-manager qemu-desktop libvirt edk2-ovmf dnsmasq vde2 bridge-utils iptables-nft dmidecode

paru -Syyu --noconfirm --needed qemu-block-gluster qemu-block-iscsi libguestfs qemu-emulators-full

#paru -Syyu --noconfirm --needed qemu-arch-extra

echo " "
echo "setup virt manager services"
sudo systemctl enable --now libvirtd.service

sudo virsh net-autostart default
sudo virsh net-start default

echo " "
echo "add user to libvirt groups "
sudo usermod -aG libvirt $USER
sudo usermod -aG libvirt-qemu $USER
sudo usermod -aG kvm $USER
sudo usermod -aG input $USER
sudo usermod -aG disk $USER

echo " "
echo " append to default libvirt.conf file"
sudo cp /etc/libvirt/libvirtd.conf /etc/libvirt/libvirtd.conf.install 
sudo cp libvirtd.conf /etc/libvirt/libvirtd.conf
