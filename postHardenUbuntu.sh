#!/usr/bin/env bash

echo "This script should be run after the harden script"

echo " "
echo "firewall rule for ssh"
sudo ufw allow ssh

echo " "
echo " copy harden ssh setup"
sudo cp issue.net /etc/issue.net
sudo cp hardenSSH.conf /etc/ssh/sshd_config.d/

echo " "
echo " install extra harden packages"
sudo apt-get install apt-show-versions htop glances git -y

echo " "
echo "get latest lynis "
cd ~
git clone https://github.com/CISOfy/lynis

echo " "
echo "run lynis to get score"

echo " "
echo "set the hostname for dns stuff"
echo "sudo hostnamectl set-hostname black.macknife.info"
echo "sudo vim /etc/hosts"
echo "127.0.0.1 black.macknife.info black"
echo " "